// parse CLI arguments
const opt = {
  valid: false
};


module.exports = (args) => {

  'use strict';

  // options already set
  if (opt.valid || !args) {
    return opt;
  }

  const
    fs = require('fs'),
    path = require('path'),
    program = require('commander'),
    pkg = require('../package.json'),
    cfg = require('../config.json'),
    util = require('./util');

  // define settings
  // remaining:
  // hkqz
  // BDEFGHIJKLNOPQRTVWXYZ
  program
    .version(pkg.version)
    .description(pkg.description)
    .option('-u, --url <url>', 'publisher page URLs (required or series)', util.checkURL)
    .option('-s, --series <file>', 'series of URLs in a file (required or URL)', util.checkStr)
    .option('-j, --urljump <num>', 'jump to this URL (1)', util.checkInt)
    .option('-c, --urlcount <num>', 'number of URLs to process (to end)', util.checkInt)
    .option('-p, --proxy <ip>', 'connect via proxy or "local" (required)', util.checkStr)
    .option('-A, --auth <user:pass>', 'proxy HTTP authenticated user and password', util.checkStr)
    .option('-d, --device <id>', 'use a specific device, such as gx6 (required)', util.checkStr)
    .option('-m, --minimal', 'enable low-bandwidth mode (Pioneer)')
    .option('-M, --maximum', 'enable high-bandwidth mode (limited caching)')
    .option('-a, --adfollow <num>', 'the maximum number of ads to process (' + cfg.followMax + ')', util.checkInt)
    .option('-n, --nofollow', 'record ads but do not follow')
    .option('-l, --language <list>', 'set a language, e.g. "en-GB,fr,*"', util.checkStr)
    .option('-f, --folder <folder>', 'log to specific folder, e.g. -f ./mylog/', util.checkStr)
    .option('-C, --country <code>', 'add a country code to log files', util.checkStr)
    .option('-i, --individual <folder>', 'use specific user profile folder, e.g. -i ./userdata/', util.checkStr)
    .option('-U, --user <user:pass>', 'user ID and password for scripts', util.checkStr)
    .option('-e, --erasecookies', 'clear all cookies before run')
    .option('-y, --yescookies', 'auto-accept cookie/privacy notices')
    .option('-g, --getip', 'record IP address in logs')
    .option('-h, --hop <num>', 'random hops within the publisher site', util.checkInt)
    .option('-w, --webhook <url>', 'request a URL callback on completion', util.checkURL)
    .option('-v, --verbose <mode>', 'logging: 0=silent, 1=normal, 2=verbose', util.checkInt)
    .option('-r, --result', 'output results JSON to stdout and webhook value')
    .option('-S, --scrape', 'publisher page screenshot and content')
    .option('-t, --test', 'test prelander scripts against URLs')
    .option('-x, --exclude <list>', 'exclude ad networks', util.checkStr)
    .option('-b, --blockads', 'enable Chrome ad-blocker')
    .option('-o, --open', 'open Chrome (disable headless mode)')
    .on('--help', () => {
      console.log('\n  Example:\n\n    g4 -u http://site1.com/ -p 1.2.3.4:5555 -d ip7\n    (use quotes if spaces are required, e.g. -l "en,fr,*")\n');
    })
    .parse(args);

  // clean
  let
    err = [],
    series = program.series || '';

  // define values
  opt.g4dir = path.resolve(__dirname + path.sep + '..') + path.sep;
  opt.g4lib = path.resolve(opt.g4dir + 'lib') + path.sep;
  opt.url = (program.url || '').split(',');
  opt.proxy = (program.proxy || '').toLowerCase();
  opt.auth = program.auth || null;
  opt.deviceCode = (program.device || 'x').toLowerCase();
  opt.device = cfg.device[opt.deviceCode] || null;
  opt.nofollow = !!program.nofollow;
  opt.adfollow = Math.max(1, program.adfollow || cfg.followMax);
  if (opt.nofollow && !program.adfollow) opt.adfollow = 100;
  opt.language = program.language || cfg.browser.language;
  opt.logfolder = path.resolve(program.folder || opt.g4dir + 'log') + path.sep;
  opt.country = program.country || null;
  opt.userfolder = program.individual || false;
  opt.user = util.splitUserPassword(program.user);
  opt.erasecookies = !!program.erasecookies;
  opt.yescookies = !!program.yescookies;
  opt.getIP = !!program.getip;
  opt.hop = Math.abs(program.hop || 0);
  opt.webhook = program.webhook || '';
  opt.verbose = Math.min(Math.max(0, program.verbose === null ? 1 : program.verbose), 2);
  opt.result = !!program.result;
  opt.scrape = !!program.scrape;
  opt.test = !!program.test;
  opt.exclude = [];
  opt.blockads = !!program.blockads;
  opt.open = !!program.open;
  opt.minimal = !!program.minimal;
  opt.maximum = opt.scrape || (!opt.minimal && !!program.maximum);
  opt.cachefolder = path.resolve(opt.g4dir + cfg.cache.folder) + path.sep;
  opt.cache = {};

  // script containers (loaded on first use)
  opt.postpublisher = false;
  opt.prelander = false;

  // auto-accept strings
  opt.autotext = false;
  opt.autocontainer = false;

  // get series of URLs from file
  if (series) {

    series = path.resolve(series);

    if (!series || !util.fileAccess(series, 'R_OK')) {
      err.push('failed to read URL file ' + series);
    }
    else {
      opt.url = opt.url.concat(fs.readFileSync(series, 'utf8').replace(/\r/g, '').split('\n'));
    }

  }

  // validate URLs
  opt.url = opt.url
    .map(util.checkURL)
    .filter((u, i, list) => !!u && list.indexOf(u) === i);

  // limit URL list to start and count
  let
    uStart = Math.abs(program.urljump || 1) - 1,
    uDo = Math.abs(program.urlcount ? program.urlcount : opt.url.length);

  opt.url = opt.url.slice(uStart, uStart + uDo);

  // check URL
  if (!opt.url.length) {
    err.push('no URLs to test');
  }

  // check proxy
  if (!opt.proxy || (opt.proxy !== 'local' && !util.verifyIP(opt.proxy) && !util.verifyDomain(opt.proxy))) {
    err.push('set proxy to IP or \'local\'');
  }
  if (opt.proxy === 'local') opt.proxy = null;

  // HTTP/proxy authentication
  if (opt.auth) {

    opt.auth = util.splitUserPassword(opt.auth);

    if (opt.auth.username) opt.auth.response = 'ProvideCredentials';
    else err.push('set valid authentication user:password');

  }

  // check device
  if (!opt.device) {
    let dev = [];
    for (let d in cfg.device) dev.push(`${util.pad(d,6,' ')}: ${cfg.device[d].name}`);
    err.push('set valid device: \n    ' + dev.join('\n    '));
  }
  else {

    // avoid own device in headless mode
    if (!opt.device.useragent && !opt.open) {
      err.push('do not use "--device own" without "--open"', 'a detectable "Headless" useragent would be sent');
    }

  }

  // process auto-accept strings
  if (opt.yescookies) {

    // format and remove duplicates
    let findFormat = str => Array.from(new Set(str.toLowerCase().split(',').map(t => t.replace(/\W/g,'')))).join(',');

    opt.autolink = findFormat(cfg.autoaccept.link);
    opt.autocontainer = findFormat(cfg.autoaccept.container);

  }


  // process exclude list
  let exDone = [];
  (program.exclude || '').split(',').forEach(x => {

    x = x.trim();
    if (!x) return;

    if (cfg.exclude[x]) {

      if (!exDone.includes(x)) {
        opt.exclude = opt.exclude.concat(cfg.exclude[x]);
        exDone.push(x);
      }

    }
    else {
      err.push('unrecognised exclude name "' + x + '"');
    }

  });

  // check log folder
  if (!err.length && (!util.makePath(opt.logfolder) || !util.fileAccess(opt.logfolder, 'W_OK'))) {
    err.push('unable to write to log folder', opt.logfolder);
  }

  // check user folder
  if (!err.length && opt.userfolder) {

    opt.userfolder = path.resolve(opt.userfolder) + path.sep;

    if (!util.makePath(opt.userfolder) || !util.fileAccess(opt.userfolder, 'W_OK')) {
      err.push('unable to write to user profile folder', opt.userfolder);
    }

  }

  if (!err.length) {

    // check cache folder
    if (!util.makePath(opt.cachefolder) || !util.fileAccess(opt.cachefolder, 'W_OK')) {
      err.push('unable to write to cache folder', opt.cachefolder);
    }
    else {

      // create cache folders
      let sub = cfg.cache.sub.every(s => {
        opt.cache[s] = opt.cachefolder + s + path.sep;
        return util.makePath(opt.cache[s]);
      });

      if (!sub) err.push('unable to create cache sub-folders', opt.cachefolder);

    }

  }

  // show errors
  if (err.length) {
    console.log('\n  Errors:\n\n    ' + err.join('\n    ') + '\n\n  use --help for assistance\n');
  }
  else {

    // set verbosity
    util.setLogLevel(opt.verbose);

    // set valid
    opt.valid = true;

  }

  return opt;

};
