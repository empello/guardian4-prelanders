// process all advert links
// returns count of successful journeys

module.exports = (opt) => {

  'use strict';

  const
    cfg = require('../config.json'),
    workerExec = opt.g4lib + 'worker-adfollow';

  // process all adverts
  async function start(adList, pubURL) {

    return new Promise(async resolve => {

      let
        startTime = +new Date(),
        processing = 0,
        fork = require('child_process').fork,
        timer = setInterval(() => {

          if (!processing || +new Date() - startTime > cfg.timeout.maxProcessing) {

            clearInterval(timer);

            // kill all workers
            for (let a = 0, ad; a < adList.length && (ad = adList[a]); a++) {
              if (ad && ad.worker && ad.worker.kill) ad.worker.kill();
            }

            resolve(true);
          }

        }, 2000);

      processing = adList.length;

      // load prelander scripts on first ad found
      if (processing && !opt.prelander) {
        opt.prelander = opt.minimal ? [] : await require('./script-load')({ type: 'prelander', cache: opt.cache.prelander, user: opt.userfolder, verbose: opt.verbose });
      }

      // process all ads
      for (let a = 0; a < processing; a++) {

        setTimeout(function() { workerStart(a); }, cfg.tabDelay * a);

      }


      // initiate worker and follow advert
      function workerStart(a) {

        let ad = adList[a];
        ad.worker = fork(workerExec);
        ad.worker.on('message', workerMessage);
        ad.worker.on('error', workerClose);
        ad.worker.on('close', workerClose);

        ad.worker.send({
          opt: opt,
          ad: a,
          pub: pubURL,
          url: ad.link
        });

      }


      // worker thread message
      function workerMessage(info) {

        if (!info) return;

        let ad = adList[info.ad];
        ad.loaded = info.loaded;
        ad.log = info.log;
        ad.title = info.title;
        ad.html = info.html;
        ad.imgData = info.imgData || ad.imgData || null;
        ad.imgExt = info.imgExt || ad.imgExt || null;
        ad.screen = info.screen;
        ad.screenURL = info.screenURL;
        ad.history = info.history;
        ad.mask = info.mask;
        ad.download = info.download;
        ad.downloadFiles = info.downloadFiles;
        ad.downloadLink = info.downloadLink;
        ad.blacklisted = info.blacklisted;
        ad.bandwidth = info.bandwidth || 0;

        // kill worker
        if (ad && ad.worker && ad.worker.kill) {
          ad.worker.kill();
        }

      }

      // worker thread closed
      function workerClose() {
        processing--;
      }

    });

  }

  // public methods
  return {
    start: start
  };

};
