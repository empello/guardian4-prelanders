// client-side actions

module.exports = (() => {

  'use strict';


  // block itritations such as popups, history manipulation and Chrome headless checks
  function blockIritations(languageSet) {

    window.guardian4 = window.guardian4 || {};

    // replace window.open
    window.open = function(URL) {
      if (window.guardian4.winOpenAllow) window.location.href = URL;
      return true;
    };

    // replace window.close
    window.close = function() {
      return true;
    };

    // replace history.pushState()
    window.history.pushState = function(obj, title, url) {
      historyAlter('push', title, url);
      return true;
    };

    // replace history.replaceState()
    window.history.replaceState = function(obj, title, url) {
      historyAlter('replace', title, url);
      return true;
    };

    // report history manipulation
    function historyAlter(type, title, url) {
      url = String(url || '').trim();
      if (!url || url.charAt(0) === '#') return;
      title = String(title || 'no title').trim();
      console.log('g4:history.' + type + 'State:', JSON.stringify({ title: title, url: url }));
    }

    // headless detection: ensure window.chrome is available
    window.chrome = window.chrome || {'app':{'isInstalled':false},'webstore':{'onInstallStageChanged':{},'onDownloadProgress':{}},'runtime':{'PlatformOs':{'MAC':'mac','WIN':'win','ANDROID':'android','CROS':'cros','LINUX':'linux','OPENBSD':'openbsd'},'PlatformArch':{'ARM':'arm','X86_32':'x86-32','X86_64':'x86-64'},'PlatformNaclArch':{'ARM':'arm','X86_32':'x86-32','X86_64':'x86-64'},'RequestUpdateCheckStatus':{'THROTTLED':'throttled','NO_UPDATE':'no_update','UPDATE_AVAILABLE':'update_available'},'OnInstalledReason':{'INSTALL':'install','UPDATE':'update','CHROME_UPDATE':'chrome_update','SHARED_MODULE_UPDATE':'shared_module_update'},'OnRestartRequiredReason':{'APP_UPDATE':'app_update','OS_UPDATE':'os_update','PERIODIC':'periodic'}}};

    // headless detection: navigator.webdriver returns undefined
    if (navigator.webdriver) {
      Object.defineProperty(navigator, 'webdriver', {
        get: () => undefined
      });
    }

    // headless detection: navigator.languages returns array
    if (!navigator.languages || !navigator.languages.length) {
      Object.defineProperty(navigator, 'languages', {
        get: () => languageSet.split(',')
      });
    }

    // headless detection: navigator.plugins.length
    if (!navigator.plugins || !navigator.plugins.length) {
      Object.defineProperty(navigator, 'plugins', {
        get: () => [
          { name: 'pluginone', description: 'Plugin One', filename: 'pluginone', length: 1 },
          { name: 'plugintwo', description: 'Plugin Two', filename: 'plugintwo', length: 1 }
        ]
      });
    }

    // headless detection: WebGL
    const getParameter = WebGLRenderingContext.getParameter;
    WebGLRenderingContext.prototype.getParameter = function(parameter) {

      // UNMASKED_VENDOR_WEBGL
      if (parameter === 37445) {
        return 'Intel Open Source Technology Center';
      }

      // UNMASKED_RENDERER_WEBGL
      if (parameter === 37446) {
        return 'Mesa DRI Intel(R) Ivybridge Mobile';
      }

      return getParameter(parameter);
    };

    // headless detection: navigator.permissions
    const originalQuery = window.navigator.permissions.query;
    window.navigator.permissions.query = (parameters) => (
      parameters.name === 'notifications' ?
        Promise.resolve({ state: Notification.permission }) :
        originalQuery(parameters)
    );

    // prevent unload operations
    window.addEventListener('beforeunload', winStop, false);
    window.addEventListener('unload', winStop, false);

    function winStop(w) {
      w.stopImmediatePropagation();
      w.stopPropagation();
    }

  }


  // auto-accept cookie/privacy pop-ups
  function autoAccept(linkText, containerText) {

    // to array
    linkText = linkText.split(',');
    containerText = containerText.split(',');

    // index visible text
    let cDoc = document, textNode = {}, ret = '';
    recurseDOM(cDoc.body);

    // analyse all link items
    linkText.forEach(text => {

      // no match
      if (!textNode[text]) return;

      // analyse all nodes
      textNode[text].forEach(node => {

        // find content of highest parent
        let n = node;
        while (n.parentElement !== cDoc.body) n = n.parentElement;
        let content = normaliseStr(n.textContent);

        // click if container has keywords
        if (containerText.find(str => content.includes(str))) {

          if (doClick(node)) ret = (ret ? ret + ',' : '') + text;

        }

      });

    });

    // return clicked items
    return ret;


    // recurse DOM and index text items
    function recurseDOM(node) {

      // store visible text nodes
      if (node.nodeType === 3 && node.parentElement !== cDoc.body && !!node.parentElement.offsetHeight) {

        let nv = normaliseStr(node.nodeValue);
        if (nv) {
          textNode[nv] = textNode[nv] || [];
          textNode[nv].push(node.parentElement);
        }

      }

      // recurse child nodes
      for (let cn = 0; cn < node.childNodes.length; cn++) {
        recurseDOM(node.childNodes[cn]);
      }

    }


    // normalise string - no non-word characters, lowercase
    function normaliseStr(str) {
      return String(str).replace(/\W/g,'').toLowerCase();
    }


    // emulate mouse/touch click
    function doClick(node) {

      let evt = cDoc.createEvent('MouseEvent');
      evt.initMouseEvent(
        'click',
        true, true, window, null,
        0, 0, 0, 0,
        false, false, false, false,
        0, null
      );
      node.dispatchEvent(evt);

      return true;

    }


  }


  // raise element above all others
  function showElement(selector) {

    let node = document.querySelector(selector);
    if (!node) return;

    // .style name, css name, new value
    let prop = new Map([
      [ 'position', { css: 'position',  set: 'absolute' } ],
      [ 'visibility', { css: 'visibility',  set: 'visible' } ],
      [ 'zIndex',   { css: 'z-index',   set: 2147483647 } ]
    ]);

    // stylesheet ID
    let cssId = 'g4ShowCSS';

    if (node.dataset.g4show) {

      // remove stylesheet
      let css = document.getElementById(cssId);
      if (css) {
        css.parentNode.removeChild(css);
      }

      // previous state
      let state = JSON.parse(node.dataset.g4show);

      // revert
      prop.forEach((v, k) => { node.style[k] = state[k]; });

      // discard state
      node.dataset.g4show = '';

    }
    else {

      // create stylesheet (all elements hidden)
      let css = document.createElement('style');
      css.id = cssId;
      document.head.appendChild(css);
      css.sheet.insertRule('* { visibility: hidden; z-index: 0; }');

      // initial state
      let state = {};

      // get state and set new
      prop.forEach((v, k) => {
        state[k] = appliedStyle(node, v.css);
        node.style[k] = v.set;
      });

      // store state
      node.dataset.g4show = JSON.stringify(state);

    }

    // get computed style
    function appliedStyle(node, prop) {
      return window.getComputedStyle(node, null).getPropertyValue(prop);
    }

  }


  // locate adverts in publisher page
  function findAds(domain, blacklist) {

    let
      isBlacklisted = blacklistDetect(domain, blacklist),
      js = 'javascript:',
      adList = [],
      minSize = 30,         // minimum width/height of iframe
      minFrameImg = 0.8,    // mimimum ratio of image to iframe size
      iframeFollow = true;

    // find standard links
    Array.from(document.getElementsByTagName('a')).forEach(ad => adListAdd(parseLink(ad)) );

    // find iframed adverts
    Array.from(document.getElementsByTagName('iframe')).forEach(ad => adListAdd(parseIframe(ad)) );

    // return advert list
    return JSON.stringify(adList);


    // add a new link/iframe
    function adListAdd(ad) {
      if (!ad) return false;

      // ad already registered?
      let newAd = adList.every(e => ((ad.link && ad.link !== e.link) || (ad.fsrc && e !== ad.fsrc)) );
      if (newAd) adList.push(ad);

      return !newAd;
    }


    // parse a standard link
    function parseLink(ad) {

      let
        href = (ad.href || '').trim(),
        isVisible = !!ad.offsetHeight,
        c = ad.children.length - 1;

      // is link or child element visible?
      while (!isVisible && c >= 0) {
        isVisible = !!ad.children[c].offsetHeight;
        c--;
      }

      if (
        isVisible && href && !isBlacklisted(href) &&
        (
          (href.length > 20 && href.startsWith(js)) ||
          (ad.search || ad.pathname.length > 50)
        )
      ) {

        let
          cont = (ad.textContent || '').replace(/\s+/g, ' ').trim(),
          imglist = ad.getElementsByTagName('img'),
          img = (imglist.length === 1 ? imglist[0] : null);

        if (cont || (img && !isBlacklisted(img.src))) {

          let box = ad.getBoundingClientRect();

          return {
            type: 'link',
            fsrc: null,
            link: href,
            text: cont,
            imgS: (img ? (img.src || '').trim() : null),
            imgW: (img ? img.width : 0),
            imgH: (img ? img.height : 0),
            imgD: (img ? img.naturalWidth * img.naturalHeight : 0), // natural image dimensions
            posX: box ? window.scrollX + box.left : 0,
            posY: box ? window.scrollY + box.top : 0
          };

        }

      }

    }


    // parse an iframe
    // only works if browser started with --disable-web-security
    function parseIframe(ad) {

      let
        adSet = [],
        sel = null,
        frmW = ad.offsetWidth,
        frmH = ad.offsetHeight,
        frmD = frmW * frmH,
        box = ad.getBoundingClientRect(),
        posX = box ? window.scrollX + box.left : 0,
        posY = box ? window.scrollY + box.top : 0;

      // iframe must be a minimum size
      if (frmW < minSize || frmH < minSize) return;

      // outer iframe selector
      if (ad.id) sel = `[id="${ad.id}"]`; // works if ID contains slashes
      else if (ad.name) sel = `[name="${ad.name}"]`;
      else if (ad.src) sel = `[src="${ad.src}"]`;
      if (sel) sel = 'iframe' + sel;

      recurseIframes(ad);

      if (adSet.length) {

        // sort by most prominent image/link
        adSet.sort((b, a) => (a.imgD + a.text.length) - (b.imgD + b.text.length));
        adSet[0].type = 'frame';
        adSet[0].fsrc = ad.src;
        adSet[0].fsel = sel;
        adSet[0].frmW = frmW;
        adSet[0].frmH = frmH;
        adSet[0].frmD = frmD;
        adSet[0].posX = posX;
        adSet[0].posY = posY;

        // remove image if it does not cover most of frame
        if (
          adSet[0].imgW / frmW < minFrameImg ||
          adSet[0].imgH / frmH < minFrameImg ||
          adSet[0].imgW * adSet[0].imgH / frmD < minFrameImg
        ) {
          adSet[0].imgS = null;
        }

        return adSet[0];

      }
      else if (sel && !isBlacklisted(ad.src)) {

        return {
          type: 'frameclick',
          link: sel,
          fsel: sel,
          fsrc: ad.src || '',
          frmW: frmW,
          frmH: frmH,
          frmD: frmD,
          posX: posX,
          posY: posY
        };

      }
      else return;

      // recurse iframes
      function recurseIframes(node) {

        if (!iframeFollow || node.offsetWidth < minSize || node.offsetHeight < minSize) return;

        try {
          var
            iLink = node.contentDocument.getElementsByTagName('a'),
            childFrame = node.contentDocument.getElementsByTagName('iframe');
        }
        catch(e) {
          console.log('SECURITY ERROR: unable to follow iframes - use headless mode', e);
          iframeFollow = false;
          return;
        }

        // parse links
        Array.from(iLink).forEach(link => {
          link = parseLink(link);
          if (link) adSet.push(link);
        });

        // do sub-iframes
        for (let i = 0; i < childFrame.length; i++) recurseIframes(childFrame[i]);

      }

    }


    // is URL blacklisted?
    function blacklistDetect(domain, blacklist) {

      blacklist = []
        .concat(new RegExp('^https{0,1}:\\/\\/([^\\/]+\\.)*' + domain + '[./]', 'i'), /^mailto/i, /^javascript:\s*(void|.+\.submit)\s*\(/i)
        .concat(blacklist.split(';;').map(i => new RegExp(i.replace(/\./g, '\\.').replace(/\//g, '\\/'), 'i')));

      return function (url) {

        // format URL and remove querystring
        url = (url || '').trim().replace(/\?.+$/, '');

        // blacklist current domain
        let bl = false, b = 0;

        // in URL blacklist?
        while (!bl && b < blacklist.length) {
          bl = blacklist[b].test(url);
          b++;
        }

        return bl;

      };

    }

  }


  // detect iframe masking
  // minVisible - what proportion of a non-scrolling iframe must be visible?
  function findMasks(domain, blacklist, minVisible = 0.85, minOpacity = 0.2, maxOverlay = 0.02) {

    // parse blacklist
    let isBlacklisted = blacklistDetect(domain, blacklist);

    // find off-site iframes
    return recurseIframes(document);

    // recurse iframes
    function recurseIframes(node, path = []) {

      let ret = '';
      if (node.contentDocument) {
        path.unshift(node); // parent iframes
        node = node.contentDocument; // current document
      }

      var n, f = node.getElementsByTagName('iframe');

      for (n = 0; n < f.length; n++) {

        // check frame
        if (f[n].src && !isBlacklisted(f[n].src)) {
          ret += checkFrame([].concat(f[n], path));
        }

        // recurse child iframes
        ret += recurseIframes(f[n], path);

      }

      return ret;

    }


    // check iframe for masking
    function checkFrame(path) {

      let
        ret = '',
        frame = path[0],
        visible = getVisibility([].concat(path)); // pass cloned path

      // visibility problem
      if (visible < minVisible) {
        ret = logMask(frame, Math.round((1 - visible) * 100) + '% area hidden');
      }

      // opacity problem
      if (!ret) {

        let opacity = getOpacity([].concat(path));

        if (opacity < minOpacity) {
          ret = logMask(frame, Math.round((1 - opacity) * 100) + '% transparent');
        }
      }

      // overlay problem
      if (!ret) {

        let overlay = getOverlays([].concat(path));

        if (overlay > maxOverlay) {
          ret = logMask(frame, Math.round(overlay * 100) + '% element over/underlays');
        }

      }

      return ret;

    }


    // check iframe dimensions
    function getVisibility(path) {

      let visible = 1;

      while (path.length) {

        // next frame
        let frame = path.shift();

        // frame has scrollbars - ignore
        if ((frame.getAttribute('scrolling') || '').toLowerCase() !== 'no' && appliedStyle(frame, 'overflow') !== 'hidden') continue;

        // get inner page dimensions
        let fdoc = frame.contentDocument.body;
        if (!fdoc || !fdoc.scrollWidth || !fdoc.scrollHeight) continue;

        // get frame size
        let box = frame.getBoundingClientRect();

        // visibility level
        visible *= (box.width * box.height) / (fdoc.scrollWidth * fdoc.scrollHeight);

      }

      return visible;

    }


    // check iframe opacity
    function getOpacity(path) {

      let opacity = 1;

      while (path.length) {

        // next frame
        let node = path.shift();

        // calculate opacity of all ancestor nodes
        while (node) {
          opacity *= appliedOpacity(node);
          node = node.parentElement;
        }

      }

      return opacity;

    }


    // check iframe overlays
    function getOverlays(path) {

      let
        overlay = 0,
        main, mdim, msize;

      while (path.length) {

        // next frame
        let
          frame = path.shift(),
          box = frame.getBoundingClientRect();

        // main frame dimensions
        if (!main) {
          main = frame;
          mdim = { top: box.top, bottom: box.bottom, left: box.left, right: box.right };
          msize = box.width * box.height;
        }
        else {
          mdim.top += box.top;
          mdim.bottom += box.top;
          mdim.left += box.left;
          mdim.right += box.left;
        }

        // find overlaying elements
        recurseDoc(frame.ownerDocument.body, frame);

      }

      return (overlay / msize);


      // recurse the document
      function recurseDoc(node, frame) {

        let cNode = node.children;
        for (let c = 0; c < cNode.length; c++) {

          let n = cNode[c];

          // ignore current frame and ancestors
          let m = frame, ignore = false;
          while (!ignore && m) {
            if (n === m) ignore = true;
            m = m.parentElement;
          }

          if (!ignore) {

            // get dimensions
            let box = n.getBoundingClientRect();

            let o = (
              Math.max(0, Math.min(mdim.right, box.right) - Math.max(mdim.left, box.left)) *
              Math.max(0, Math.min(mdim.bottom, box.bottom) - Math.max(mdim.top, box.top))
            );

            overlay += o;

            // debug
            // if (o) console.log(n.nodeName, n.id || '', n.className || '', o);

          }

          recurseDoc(n, frame);

        }

      }

    }


    // log a new masking type
    function logMask(frame, type) {
      return (
        '[MASK : ' + type + '\n' +
        (frame.id ? ' id   : ' + frame.id + '\n' : '') +
        ' src  : ' + frame.src +
        '\n]\n'
      );
    }


    // get computed style
    function appliedStyle(node, prop) {
      return window.getComputedStyle(node, null).getPropertyValue(prop);
    }


    // get current opacity (0 - 1)
    function appliedOpacity(node) {

      // CSS opacity value
      let o = parseFloat(appliedStyle(node, 'opacity'));

      // CSS filter: opacity value
      let filter = appliedStyle(node, 'filter').match(/opacity\((\s*[\d|.]+)/i);
      if (filter && filter.length && filter.length === 2) {
        o *= parseFloat(filter[1]);
      }

      return o;

    }


    // is URL blacklisted?
    function blacklistDetect(domain, blacklist) {

      blacklist = []
        .concat(new RegExp('^https{0,1}:\\/\\/([^\\/]+\\.)*' + domain + '[./]', 'i'), /^mailto/i, /^javascript:\s*(void|.+\.submit)\s*\(/i)
        .concat(blacklist.split(';;').map(i => new RegExp(i.replace(/\./g, '\\.').replace(/\//g, '\\/'), 'i')));

      return function(url) {

        // format URL and remove querystring
        url = (url || '').trim().replace(/\?.+$/, '');

        // blacklist current domain
        let bl = false, b = 0;

        // in URL blacklist?
        while (!bl && b < blacklist.length) {
          bl = blacklist[b].test(url);
          b++;
        }

        return bl;

      };

    }

  }


  // locate downloads in a landing page
  function findDownloads(filetype) {

    if (!filetype) return '';

    // file download regular expression
    let reFile = new RegExp('https?:\\/\\/.+\\/.+\\.(' + filetype + ')([?#].+){0,}$', 'i');

    // do all documents
    return recurseIframes(document);

    // recurse iframes
    function recurseIframes(node) {

      let ret = '';

      // in iframe
      if (node.contentDocument) {
        node = node.contentDocument;
      }

      // test all links
      let a = node.getElementsByTagName('a');
      for (let i = 0; i < a.length; i++) {
        let h = a[i].href;
        if (h && reFile.test(h)) ret += h + '\n';
      }

      // recurse child iframes
      let f = node.getElementsByTagName('iframe');
      for (let n = 0; n < f.length; n++) {
        ret += recurseIframes(f[n]);
      }

      return ret;

    }

  }


  // public methods
  return {
    blockIritations: blockIritations,
    autoAccept: autoAccept,
    showElement: showElement,
    findAds: findAds,
    findMasks: findMasks,
    findDownloads: findDownloads
  };

})();
