// load scripts (postpublisher or prelander)
'use strict';

module.exports = ({ type, cache, user, verbose }) => {

  const
    cfg = require('../config.json'),
    path = require('path'),
    util = require('./util'),
    sep = path.sep,
    cacheFile = path.resolve(cache + 'parsed.json'),
    scPath = path.resolve(__dirname + sep + '..' + sep + type);

  // set random seed if using --individual folder
  util.randomSeed(user);

  const
    autoFill = {
      firstname:  util.strRandom(0, true),
      lastname:   util.strRandom(0, true),
      age:        String(Math.floor(util.randomizer() * 48) + 18),
      email:      util.emailRandom(),
      telephone:  '0' + (Math.floor(util.randomizer() * 9000000000) + 1000000000),
      msisdn:     String(Math.floor(util.randomizer() * 9000000000) + 1000000000),
      password:   String(util.strRandom(5, true) + util.strRandom(4, true) + Math.round(util.randomizer() * 100))
    };

  // generated autofill values
  autoFill.name = autoFill.firstname + ' ' + autoFill.lastname;
  autoFill.mail = autoFill.email;
  autoFill.tel = autoFill.telephone;
  autoFill.phone = autoFill.telephone;

  let
    fileList = [],
    script = [],
    cacheIdx = [],
    cacheFound = [],
    cacheDate,
    updates = 0;

  util.setLogLevel(verbose);
  util.log(1, 'loading', type, 'scripts');

  return new Promise(async resolve => {

    // load cache file, recurse script folder and process new
    await loadCache();
    await read(scPath);
    await processScript();

    async function processScript() {

      if (fileList.length) {

        // parse next file on queue
        let
          file = fileList.shift(),
          scrContent = await load(file);

        if (scrContent.length) {

          updates++;
          let i = cacheIdx.indexOf(file);
          if (i < 0) {
            // add new script
            cacheFound[script.length] = true;
            script.push({ file, script: scrContent });
          }
          else {
            // replace existing script
            cacheFound[i] = true;
            script[i] = { file, script: scrContent };
          }

        }

      }
      else {

        // remove deleted scripts
        let i = 0;
        while (i < script.length) {
          if (cacheFound[i]) i++;
          else {

            // remove item
            script.splice(i, 1);
            cacheFound.splice(i, 1);
            updates++;

          }
        }

        if (script.length) {
          util.log(2, type, 'scripts loaded:', script.length, (updates ? '- updated: ' + updates : ''));

          // save scripts to cache
          if (updates) await util.fileWrite(cacheFile, JSON.stringify(script));
        }

        resolve(script);
        return;
      }

      // do next item
      process.nextTick(async () => await processScript());

    }

  });


  // load scripts from cache
  async function loadCache() {

    let cache = await util.pathInfo(cacheFile);
    if (cache) {

      script = require(cacheFile);
      cacheDate = cache.mTime;

      // index scripts
      script.filter((script, i) => { cacheIdx[i] = script.file; });

    }
    else cacheDate = 0;

  }


  // recurse directories
  async function read(fn) {

    let info = await util.pathInfo(fn);
    if (!info) return;

    if (info.isDir) {

      // get items (ignore those starting ~)
      let sub = await util.pathRead(fn);
      await Promise.all(sub.map(async s => {
        if (!s.startsWith('~')) await read(fn + sep + s);
      }));

    }
    else if (info.isFile) {

      // add file to queue
      if (info.mTime > cacheDate || !cacheIdx.includes(fn)) {
        fileList.push(fn);
      }
      else {

        // file found
        let i = cacheIdx.indexOf(fn);
        if (i >= 0) cacheFound[i] = true;

      }

    }

  }


  // load a script
  async function load(fn) {

    let
      script = [],
      scriptError = [],
      landSet = false,
      proxySet = false,
      reQuotes = /^["|'|`]|["|'|`]$/g,
      scr = await util.fileRead(fn);

    // parse script into commands
    scr = ('\n' + scr)
      .replace(/\r/g, '')
      .replace(/\n#.*/g, '')
      .replace(/\n+/g, '[||]')
      .replace(/\s+/g,' ')
      .trim()
      .split('[||]')
      .filter(a => !!a.trim().length);

    // parse lines
    for (let l = 0; l < scr.length; l++) {

      let
        err = [],
        line = scr[l],
        ps = line.indexOf(' '),
        argExpected = 1;

      if (ps < 0) ps = line.length;

      let
        cmd = line.slice(0, ps).toUpperCase(),
        arg = line
          .slice(ps+1)
          .trim()
          .replace(reQuotes, '')
          .split()
          .filter(a => !!a.trim().length);

      switch (cmd) {

        case 'LAND':
          arg = argRegEx(arg);
          break;

        case 'PROXY':
          if (proxySet) err.push('No more than one PROXY command can be defined: ' + line);
          arg = argRegEx(arg);
          break;

        case 'HAS':
          break;

        case 'FRAME':
          break;

        case 'FIELD':
          argExpected = 2;
          if (arg.length) {

            arg = arg[0]
              .split('|')
              .map(a => a.trim().replace(reQuotes, ''))
              .filter(a => !!a.trim().length);

            // parse special field values
            if (arg[1] && arg[1].includes('<')) {

              for (let f in autoFill) {
                arg[1] = arg[1].replace(new RegExp('<' + f + '>', 'ig'), autoFill[f]);
              }

            }

          }

          break;

        case 'CHECK':
          argExpected = 2;
          if (arg.length) {

            arg = arg[0]
              .split('|')
              .map(a => a.trim().replace(reQuotes, ''))
              .filter(a => !!a.trim().length);

            if (arg.length === 1) arg[1] = true;
            else {
              let v = arg[1].toLowerCase();
              arg[1] = !(v === 'off' || v === 'false' || v === '0');
            }
          }

          break;

        case 'TOUCH':
          break;

        case 'CLICK':
          if (script.length && script[script.length - 1].cmd === 'SHOT') err.push('Unnecessary SHOT before CLICK command: ' + line);
          break;

        case 'RUN':
          break;

        case 'SHOT':
          if (script.length && script[script.length - 1].cmd === 'SHOT') err.push('Successive SHOT commands are not permitted');
          argExpected = 0;
          break;

        case 'WAIT':
          arg[0] = (arg.length ? parseInt(arg[0].replace(reQuotes, ''), 10) || 0 : 0);
          if (arg[0] < cfg.timeout.firstRequest) err.push('Unnecessary WAIT: ' + line);
          break;

        default:
          err.push('Invalid command: ' + line);
          break;

      }

      if (arg.length !== argExpected) {
        err.push('Invalid number of arguments: ' + line);
      }

      if (err.length) {

        // add command error to error list
        scriptError = scriptError.concat(err);

      }
      else {

        // add command to script
        let sc = {
          cmd: cmd,
          arg: (arg.length ? (arg.length === 1 ? arg[0] : arg) : null),
        };

        if (cmd === 'LAND' && !landSet) landSet = sc;
        else if (cmd === 'PROXY') proxySet = sc;
        else script.push(sc);

      }

    }

    if (!script.length) scriptError.push('No commands?');

    // handle LAND
    if (landSet) script.unshift(landSet);
    else scriptError.push('No LAND url set?');

    // handle PROXY
    if (proxySet) script.unshift(proxySet);

    if (scriptError.length) {

      util.log(1, type.toUpperCase(), 'ERRORS:', '\n' + fn, '\n' + scriptError.join('\n') + '\n');
      return [];

    }
    else {
      return script;
    }

  }


  // convert first argument to a regex string (LAND and PROXY commands)
  function argRegEx(arg) {

    if (arg.length) {

      if (arg[0].startsWith('/') && arg[0].endsWith('/')) {
        arg[0] = arg[0].replace(/^\/|\/$/g, '');
      }
      else {
        arg[0] = arg[0]
          .replace(/([/\\.?$-])/g, '\\$1')
          .replace(/\*+/g, '\\.*');
      }

    }

    return arg;

  }

};
