// set browser tab defaults

const
  cfg = require('../config.json'),
  util = require('./util'),
  EventEmitter = require('events').EventEmitter,
  clientActions = require('./client-actions'),

  /*
  Intercepted requests
  includes base64-encoded HTTP header and content, e.g.

  HTTP/1.1 200 OK
  Cache-Control: no-cache, no-store, must-revalidate
  Accept-Ranges: bytes
  Content-Length: 46
  Content-Type: image/svg+xml

  <svg xmlns="http://www.w3.org/2000/svg"></svg>
  */
  tiny = {
    css: 'SFRUUC8xLjEgMjAwIE9LDQpDYWNoZS1Db250cm9sOiBuby1jYWNoZSwgbm8tc3RvcmUsIG11c3QtcmV2YWxpZGF0ZQ0KQWNjZXB0LVJhbmdlczogYnl0ZXMNCkNvbnRlbnQtTGVuZ3RoOiAyOQ0KQ29udGVudC1UeXBlOiB0ZXh0L2Nzcw0KDQpib2R5e2ZvbnQtZmFtaWx5OnNhbnMtc2VyaWY7fQ==',
    png: 'SFRUUC8xLjEgMjAwIE9LDQpDYWNoZS1Db250cm9sOiBuby1jYWNoZSwgbm8tc3RvcmUsIG11c3QtcmV2YWxpZGF0ZQ0KQWNjZXB0LVJhbmdlczogYnl0ZXMNCkNvbnRlbnQtTGVuZ3RoOiA4OA0KQ29udGVudC1UeXBlOiBpbWFnZS9wbmcNCg0KiVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQAAAAA3bvkkAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAACklEQVQIHWNoAAAAggCB8KrjIgAAAABJRU5ErkJggg==',
    svg: 'SFRUUC8xLjEgMjAwIE9LDQpDYWNoZS1Db250cm9sOiBuby1jYWNoZSwgbm8tc3RvcmUsIG11c3QtcmV2YWxpZGF0ZQ0KQWNjZXB0LVJhbmdlczogYnl0ZXMNCkNvbnRlbnQtTGVuZ3RoOiA0Ng0KQ29udGVudC1UeXBlOiBpbWFnZS9zdmcreG1sDQoNCjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48L3N2Zz4='
  },

  reHttpCheck = /^(https?:\/\/)/i;

module.exports = async function(client, opt) {

  'use strict';

  util.setLogLevel(opt.verbose);

  const
    { Network, Target, Page, Runtime, DOM } = client,
    browserActions = require('./browser-actions')(opt);

  // defaults
  client.browser = null;
  client.loaded = false;
  client.loadDelay = true;
  client.saveBandwidth = false;
  client.currentURL = '';
  client.currentDomain = '';
  client.htmlRequestId = null;
  client.html = '';
  client.mainFrame = null;
  client.log = [];
  client.history = [];
  client.download = [];
  client.netReq = {};
  client.netRes = {};
  client.autoAttach = false;
  client.bandwidth = 0;
  client.lastReceive = +new Date();
  client.autoclick = false;
  client.blacklisted = false;
  client.event = new EventEmitter();

  // append a URL or event to the log
  function logAppend(item, force = false) {
    let ll = client.log.length;
    if (force || !ll || client.log[ll - 1] !== item) client.log.push(item);
  }

  // blacklist handling
  var domainBlacklisted = null;
  async function updateBlacklist(domain) {

    if (domain !== domainBlacklisted) {

      let urls = cfg.blacklist.concat(opt.exclude).map(u => `*${u}*`);

      if (domain) {
        util.log(2, 'bandwidth saving mode enabled on', domain);
        urls = urls.concat(cfg.publisherblock.map(u => `*${domain}*${u}`));
      }
      else if (domainBlacklisted) {
        util.log(2, 'bandwidth saving mode disabled on', domainBlacklisted);
      }

      domainBlacklisted = domain || '';

      await Network.setBlockedURLs({ urls });

    }

  }


  // enable processes
  await Promise.all([ Network.enable(), Page.enable(), Runtime.enable(), DOM.enable() ]);

  // page loaded event
  Page.loadEventFired(async () => {

    // will redirect occur?
    let
      delay = cfg.timeout.minRedirect,
      redir = browserActions.couldRedirect(client);

    if (redir.url) {
      delay += (redir.time * 1000);
      util.log(2, 'redirect URL detected', redir.url.slice(0,40));
    }

    client.loaded = true;

    if (client.loadDelay) await browserActions.pause(delay);

    // fire loaded event
    if (client.loaded && client.currentURL) {

      util.log(2, '\x1b[33m\x1b[1m-LOADED', client.currentURL.slice(0,60), '\x1b[0m');
      client.event.emit('loaded', client.loaded);

    }

  });


  // record bandwidth
  // should use req.encodedDataLength, but it returns zero when setRequestInterceptionEnabled is enabled?
  Network.dataReceived(async req => {
    client.bandwidth += req.dataLength;
    client.lastReceive = +new Date();
  });


  // intercept requests
  await Network.setRequestInterception({ patterns: [{ urlPattern: '*' }] });
  Network.requestIntercepted(async req => {

    let response, auth, error;

    // not http/s?
    if (req.request.url) {
      if (!reHttpCheck.test(req.request.url)) error = true;
    }

    // HTTP/proxy authentication challenge
    if (req.authChallenge && opt.auth) auth = opt.auth;

    // track download
    if (req.resourceType === 'Document' && !client.download.includes(req.request.url)) client.download.push(req.request.url);

    // block audio and video
    if (req.resourceType === 'Media' || req.resourceType === 'TextTrack') error = true;

    // tiny response required?
    if (opt.minimal && !req.isNavigationRequest && req.request.method === 'GET') {

      switch (req.resourceType) {

        case 'Stylesheet':
          // response = tiny.css;
          // causes journeys to fail on some sites?
          break;

        case 'Media':
        case 'Image':
          response = tiny.png;
          break;

        case 'Font':
          error = true;
          break;

      }

    }

    try {

      await Network.continueInterceptedRequest({
        interceptionId: req.interceptionId,
        rawResponse: response,
        authChallengeResponse: auth,
        errorReason: error ? 'Aborted' : undefined
      });

    }
    catch(e) {
      util.log(2, 'interception error', e);
    }

  });


  // record network requests
  Network.requestWillBeSent(async req => {

    let url = req && req.request && req.request.url;
    if (!url || url.startsWith('data:')) return;

    client.netReq[url] = {
      id: req.requestId,
      frameId: req.frameId
    };

    // not a download?
    let di = client.download.indexOf(url);
    if (di >= 0) client.download.splice(di, 1);

    // get main frame ID
    if (!client.mainFrame) {
      let tree = await client.Page.getResourceTree();
      client.mainFrame = tree.frameTree.frame.id;
    }

    if (req.frameId !== client.mainFrame || req.type !== 'Document') return;

    util.log(!client.currentURL || client.isMain ? 1 : 2, 'loading', url.slice(0,60));

    // publisher page being hijacked?
    let domain = util.urlDomain(url);
    if (client.isMain && client.loaded && client.currentDomain && client.currentDomain !== domain) {
      client.hijack = client.hijack || [];
      client.hijack.push(url);
      client.autoAttach = false;
      client.saveBandwidth = true;
    }

    client.loaded = false;
    client.htmlRequestId = req.requestId;
    client.currentURL = url;
    client.currentDomain = domain;
    if (!client.hijack) logAppend(url);

    // not http/s or blacklisted?
    let
      hp = reHttpCheck.test(url),
      bl = hp && cfg.blacklist.find(e => url.includes(e));

    if (!hp || bl) {
      util.log(1, 'blacklist match:', bl || url.slice(0,30));
      client.loaded = true;
      client.blacklisted = true;
      await client.Page.stopLoading();
      client.event.emit('loaded', client.loaded);
    }

    if (client.isMain && !client.saveBandwidth && !client.blacklisted && !client.hijack) {

      // is page cached?
      client.cacheFile = opt.cache.publisher + util.md5((opt.proxy || '') + '|' + opt.deviceCode + '|' + client.currentURL) + '.' + cfg.screenshot.type.ext;

      let modified = await util.fileModified(client.cacheFile);
      if (opt.maximum || !modified || modified.setDate(modified.getDate() + cfg.cache.days) < new Date()) {
        client.cacheCreate = true;
      }
      else {
        client.saveBandwidth = true;
      }

    }

    // bandwidth-saving mode
    await updateBlacklist(client.saveBandwidth && client.currentDomain);

  });

  // record network responses
  Network.responseReceived(async res => {

    if (!res || !res.requestId || !res.response) return;

    let
      rId = res.requestId,
      fId = res.frameId,
      type = res.type.toLowerCase(),
      status = res.response.status,
      isHTML = (type === 'document' && fId === client.mainFrame),
      url = res.response.url;

    // ignore 404 and server errors
    if (isHTML && status >= 400) {

      client.loaded = true;
      client.blacklisted = true;
      await client.Page.stopLoading();
      client.event.emit('loaded', client.loaded);
      return;

    }

    if (status !== 200) return;

    // record response information
    client.netRes[rId] = {
      frameId: fId,
      type: type,
      url: url,
      mime: res.response.mimeType.toLowerCase()
    };

    // record HTML request ID
    if (isHTML) {
      client.htmlRequestId = res.requestId;
    }

    // was request recorded?
    if (url && !url.startsWith('data:') && !client.netReq[url]) {

      client.netReq[url] = {
        id: rId
      };

      // missed in log?
      if (isHTML && !client.log.length) {
        client.currentURL = url;
        client.currentDomain = util.urlDomain(url);
        logAppend(url);
      }

    }

  });


  // request has finished loading
  Network.loadingFinished(async res => {

    let reqId = res && res.requestId;
    if (!reqId) return;

    // record HTML
    if (reqId === client.htmlRequestId) {
      await browserActions.getHTML(client);
    }

  });


  // console logging event
  Runtime.consoleAPICalled(log => {

    let
      arg1 = String((log.args && log.args[0] && log.args[0].value) || ''),
      arg2 = String((log.args && log.args[1] && log.args[1].value) || '');

    // G4 log: parse history
    if (arg1 && arg2 && arg1.startsWith('g4:history.')) {

      try {
        let h = JSON.parse(arg2);
        if (h) client.history.push(h);
      }
      catch (e) {
        util.log(2, 'history parse error', e, arg2);
      }

      return;
    }

    let
      type = log.type.toUpperCase(),
      msg = log.args.map(a => a.value).join(' ').trim();

    if (msg && type && type !== 'CLEAR') util.log(2, '++' + type + ': ' + msg);

  });

  // JavaScript dialog event
  Page.javascriptDialogOpening(async alert => {

    let msg = alert.type.toUpperCase() + ' DIALOG: ' + alert.message;
    logAppend(msg, true);
    util.log(1, msg);

    // close alert after random period
    await browserActions.pause();

    try {
      await Page.handleJavaScriptDialog({ accept: true });
    }
    catch(e) {
      util.log(2, 'unable to close dialog', e);
    }

  });

  if (client.isMain) {

    // target discovered
    /*
      TODO: refactor when possible
      Currently connects to opening tab, sets low bandwidth mode, closes, then reopens to record details
      Problem: unnecessary bandwidth, convoluted, impossible to record redirect history

      option 1: close tab when first URL is known. Impossible in C60: Target.targetInfoChanged not supported.
      option 2: use existing tab. Currently unreliable in C60; screenshots and information cannot be accessed, prelanders cannot be run
    */
    client.popup = [];

    Target.setDiscoverTargets({ discover: true });
    Target.setAutoAttach({ autoAttach: false, waitForDebuggerOnStart: true });

    Target.targetCreated(async t => {

      // ignore self
      if (!client.autoAttach || client.targetId === t.targetInfo.targetId || t.targetInfo.type !== 'page') return;

      // new target encountered
      let
        connect = require('./browser-connect')(opt),
        cli = await connect.connectTarget(t.targetInfo.targetId);

      if (cli) {

        // save bandwidth on popup (will reopen later)
        cli.saveBandwidth = true;

        // continue paused page
        await cli.Runtime.runIfWaitingForDebugger();

        // add target to current client
        client.popup.push(cli);

      }

      util.log(2, 'target', t.targetInfo.targetId, 'connection', (cli ? 'successful' : 'failed'));

    });

  }
  else {

    // JavaScript to run when document loads

    // stop popups and history push
    await Page.addScriptToEvaluateOnNewDocument({
      source: `(${clientActions.blockIritations})('${opt.language}');`
    });

  }

  // connected browser information
  client.browser = await client.Browser.getVersion();

  // parse versions
  if (client.browser.userAgent) {

    client.browser.uaVersion = {
      Mozilla:  util.regExract(client.browser.userAgent, /Mozilla.([\d.]+)/i),
      WebKit:   util.regExract(client.browser.userAgent, /WebKit.([\d.]+)/i),
      Chrome:   util.regExract(client.browser.userAgent, /Chrome.([\d.]+)/i),
      ChromeM: +util.regExract(client.browser.userAgent, /Chrome.(\d+)/i),
      Safari:   util.regExract(client.browser.userAgent, /Safari.([\d.]+)/i)
    };

    client.browser.uaVersion.Firefox = Math.max(66, client.browser.uaVersion.ChromeM - 7);

  }

  // user agent
  if (opt.device.useragent) {
    if (client.browser.uaVersion) opt.device.useragent = util.tokenReplace(opt.device.useragent, client.browser.uaVersion);
    await Network.setUserAgentOverride({ userAgent: opt.device.useragent });
  }

  // emulation
  await browserActions.setDeviceSize(client);

  // additional headers
  await browserActions.setHeaders(client);

  // default blocked URLs
  await updateBlacklist();

  // enable/disable ad-blocking
  if (Page.setAdBlockingEnabled) {
    await Page.setAdBlockingEnabled({ enabled: opt.blockads });
  }

  // download behaviour
  if (Page.setDownloadBehavior) {

    await Page.setDownloadBehavior({
      behavior: cfg.download.enabled ? 'allow' : 'deny',
      downloadPath: opt.cache.download
    });

  }

};
