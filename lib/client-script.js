// client-side script processing

module.exports = () => {

  'use strict';

  let
    g4data = 'g4nodeid',
    g4Node = 1,
    cDoc = document,
    textNode = {},
    textNodeDirty = true;

  // monitor DOM nodes changes
  const observer = new MutationObserver(mutations => {
    if (mutations.length) textNodeDirty = true;
  });
  observer.observe(document.body, { subtree: true, childList: true, attributes: false, characterData: true });


  // find node and return a unique selector
  function find(selector) {

    let node = findNode(selector);
    if (node) {

      let id = parseInt(node.dataset[g4data], 10) || g4Node;
      if (id === g4Node) {
        node.dataset[g4data] = id;
        g4Node++;
      }

      return `${node.nodeName.toLowerCase()}[data-${g4data}="${id}"]`;

    }
    else return null;

  }


  // set the current frame
  function setFrame(frame) {

    let
      frameN = parseInt(frame, 10),
      ret = false;

    if (frame === '#') {

      // root frame
      cDoc = document;
      ret = true;

    }
    else if (Number.isInteger(frameN)) {

      // frame number
      let i = cDoc.getElementsByTagName('iframe');
      if (frameN >= 0 && frameN < i.length) {
        cDoc = i[frameN].contentDocument;
        ret = true;
      }

    }
    else {

      // frame by ID or selector
      let i = cDoc.getElementById(frame) || cDoc.querySelector(frame);

      if (i && i.nodeName === 'IFRAME') {
        cDoc = i.contentDocument;
        ret = true;
      }

    }

    return ret;

  }


  // set field value
  function setField(selector, value) {

    let ret = false, node = findNode(selector);
    if (node) {

      let
        name = node.nodeName.toLowerCase(),
        type = (node.type || '').toLowerCase();

      if (type === 'checkbox' || type === 'radio') {
        node.checked = (value !== 'false');
        ret = true;
      }
      else if (name === 'input' || name === 'select' || name === 'textarea') {
        if (node.focus) node.focus();
        node.value = value;
        ret = true;
      }

      if (ret) {
        // trigger events
        node.dispatchEvent(new Event('click'));
        node.dispatchEvent(new Event('change'));
      }

    }

    return ret;

  }


  // emulate mouse/touch click
  // only used within child iframes
  function doClick(selector) {

    let node = findNode(selector);
    if (!node) return false;

    let evt = cDoc.createEvent('MouseEvent');
    evt.initMouseEvent(
      'click',
      true, true, window, null,
      0, 0, 0, 0,
      false, false, false, false,
      0, null
    );
    node.dispatchEvent(evt);

    return true;

  }


  // find a node by CSS selector, title or text value
  function findNode(selector = '') {

    selector = String(selector).trim();
    let node, found = null;

    if (!selector) return found;

    // matches CSS selector?
    try {
      node = cDoc.querySelectorAll(selector);
    }
    catch(e){}

    // find by text
    if (!node || !node.length) node = findText(selector);

    // find first visible node
    for (let n = 0; !found && n < node.length; n++) {
      if (isVisible(node[n])) found = node[n];
    }

    // matches <title> in head?
    if (!found) {
      node = cDoc.getElementsByTagName('title');
      if (node) {
        let text = normaliseStr(selector);
        for (let n = 0; !found && n < node.length; n++) {
          if (text === normaliseStr(node[n].textContent)) found = node[n];
        }
      }
    }

    return found;

  }


  // find text in page
  function findText(text) {

    text = normaliseStr(text);
    if (!text) return [];

    // index text
    if (textNodeDirty) {
      textNode = {};
      recurseDOM(cDoc.body);
    }

    return textNode[text] || [];

  }


  // recurse DOM and index text items
  function recurseDOM(node) {

    // store text node
    if (node.nodeType === 3) {

      let nv = normaliseStr(node.nodeValue);

      if (nv) {
        textNode[nv] = textNode[nv] || [];
        textNode[nv].push(node.parentNode);
      }

    }

    // recurse child nodes
    for (let cn = 0; cn < node.childNodes.length; cn++) {
      recurseDOM(node.childNodes[cn]);
    }

  }


  // normalise a string - no non-word characters, lowercase
  function normaliseStr(str) {
    return String(str).replace(/\W/g,'').toLowerCase();
  }


  // is a node visible?
  function isVisible(node) {
    return !!node.offsetHeight;
  }


  // public methods
  return {
    find: find,
    setFrame: setFrame,
    setField: setField,
    doClick: doClick
  };

};
