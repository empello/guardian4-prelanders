/*

  browser.js
  starts and stops a browser process
  and configures a free debugging port

*/

// static properties
const
  cfg = require('../config.json').browser,
  opt = require('./opt')(),
  util = require('./util'),
  os = require('os').platform(),
  EventEmitter = require('events').EventEmitter,
  browser = {
    error: false,
    port: null,
    process: null
  };

module.exports = (() => {

  'use strict';

  // event emitter
  const event = new EventEmitter();

  // stopping in action
  var stopping = false;

  // start new browser instance
  async function start() {

    // browser aborted?
    if (browser.error) return false;

    // browser already started?
    if (browser.process) return true;

    util.log(1, 'starting browser');

    // get port
    await findPort();
    if (!browser.port) return false;

    // generate command
    const cmd = cfg.command && cfg.command[os];
    if (!cmd) return false;

    // start child process
    let
      { spawn } = require('child_process'),
      flags = cfg.flags,
      arg = flags.default.concat(flags.debugport + browser.port);

    // private browsing if not using a profile
    if (!opt.userfolder) arg = arg.concat(flags.private);

    // user-data folder
    arg = arg.concat(flags.userdata + (opt.userfolder || opt.cache.userdata));

    // proxy IP
    if (opt.proxy) arg = arg.concat(flags.proxy + opt.proxy);

    if (!opt.open) {
      arg = arg
        .concat(flags.headless)
        .concat(flags.windowsize + opt.device.width + ',' + opt.device.height);
    }

    util.log(2, 'running:', cmd, arg.join(' '));

    browser.process = spawn(cmd, arg);

    // error
    browser.process.on('error', error);

    // disconnect
    browser.process.on('disconnect', () => {
      error('disconnected');
    });

    // close
    browser.process.on('close', code => {
      error('terminated unexpectedly', code);
    });

    // exit
    browser.process.on('close', code => {
      error('exit', code);
    });

    util.log(2, 'browser process started', browser.process.pid);
    return true;


    // browser crash?
    function error(type, code = 1) {
      if (stopping) return;
      browser.error = true;
      browser.process = null;
      util.log(1, 'browser process', type, code);
      event.emit('close', code);
    }

  }


  // stop browser
  function stop() {

    if (browser.process) {

      util.log(1, 'terminating browser process', browser.process.pid);
      stopping = true;

      if (os.includes('win')) {

        // kill Windows process
        try {
          let { spawn } = require('child_process');
          spawn('taskkill', ['/pid', browser.process.pid, '/f', '/t']);
        }
        catch(e) {
          util.log(2, 'termination failed:', e);
        }

      }
      else {

        // kill Linux/MacOS process
        try {
          browser.process.kill('SIGINT');
        }
        catch(e) {
          util.log(2, 'termination failed:', e);
        }

      }

      browser.process = null;

    }

  }


  // get the next available debugging port
  function findPort() {

    return new Promise(resolve => {

      // port already allocated
      if (browser.port) {
        resolve(browser.port);
        return;
      }

      // find unallocated port
      util.log(2, 'locating unallocated TCP port');

      require('portscanner').findAPortNotInUse(cfg.portcheck.portStart, cfg.portcheck.portEnd, cfg.portcheck.host, (err, freePort) => {

        if (err || !freePort) {
          util.log(2, 'no spare TCP port');
        }
        else {
          browser.port = freePort;
          util.log(2, 'using TCP port', browser.port);
        }

        // update opt settings
        opt.port = browser.port;
        resolve(browser.port);

      });

    });

  }


  // return current port
  function port() {
    return browser.port;
  }


  // public methods
  return {
    start: start,
    stop: stop,
    port: port,
    event: event
  };


})();
