// process a single publisher URL
// returns an adList array
let clearCookies = true;

module.exports = opt => {

  'use strict';

  // load modules
  const
    cfg = require('../config.json'),
    util = require('./util'),
    browserConnect = require('./browser-connect')(opt),
    browserActions = require('./browser-actions')(opt),
    scriptProcess = require('./script-process')(opt, 'postpublisher'),
    clientActions = require('./client-actions');

  util.setLogLevel(opt.verbose);


  // start processing
  function start(url) {

    util.log(1, '_'.repeat(60));
    util.log(1, '\x1b[33m\x1b[1m' + (opt.test ? 'TEST:' : 'PUBLISHER:') + '\x1b[0m');
    util.log(1, '\x1b[33m\x1b[1m' + url.slice(0,60) + '\x1b[0m');
    util.log(1, '_'.repeat(60));

    return new Promise(async resolve => {

      // create publisher tab
      let
        timer = setTimeout(() => {
          // processing timeout
          resolve({ publisher: publisher || null, adList: adList || [] });
        }, cfg.timeout.loadPage),
        adList = [],
        publisher = await browserConnect.mainTarget();

      // no publisher page?
      if (!publisher || !publisher.log) {
        util.log(1, 'publisher tab could not be opened?');
        clearTimeout(timer);
        resolve(false);
        return;
      }

      // get IP address in new tab
      if (opt.getIP) {
        util.log(1, 'finding IP address');
        let ipTab = await browserConnect.newTarget(false);
        if (ipTab) {
          ipTab.loadDelay = false;
          await browserActions.loadURL(ipTab, cfg.getIPurl);
          if (ipTab.html) publisher.ipAddress = ipTab.html;
          await browserConnect.closeTarget(ipTab);
        }
      }

      // clear cookies
      if (clearCookies && (opt.erasecookies || !opt.userfolder)) {
        await browserActions.clearCookies(publisher);
        clearCookies = false;
      }

      // set Guardian identification cookie
      if (cfg.idcookie && cfg.idcookie.domains && cfg.idcookie.domains.length) {

        let
          pkg = require('../package.json'),
          name = String(cfg.idcookie.name || pkg.name || 'g'),
          value = String(cfg.idcookie.value || pkg.version || '1');

        await Promise.all(cfg.idcookie.domains.map(async (domain) => {

          await publisher.Network.setCookie({
            domain: domain,
            name: name,
            value: value
          });

        }));

      }

      // testing mode
      if (opt.test) {

        // return fake advert
        clearTimeout(timer);
        publisher.currentURL = url;
        resolve({ publisher, adList: [
          {
            type: 'test',
            link: url
          }
        ]});

        return;
      }

      // load publisher page
      util.log(1, 'loading publisher page', url);

      publisher.autoAttach = true;

      let hop = opt.hop + 1;
      do {

        hop--;

        publisher.saveBandwidth = opt.minimal || (!opt.maximum && hop);
        await browserActions.loadURL(publisher, url);

        // loading failed
        if (!publisher.loaded) {
          clearTimeout(timer);
          resolve({ publisher, adList });
          return;
        }

        if (!hop) await browserActions.randomScroll(publisher);

        // hijacked?
        if (publisher.hijack) {
          util.log(1, '\x1b[33m[----- HIJACK -----]\x1b[0m');

          // wait for page load completion
          await browserActions.loadURL(publisher);

          clearTimeout(timer);
          resolve({ publisher, adList: [
            {
              type: 'hijack',
              link: publisher.currentURL
            }
          ]});
          return;
        }

        // postpublisher script processing (chained scripts are never repeated)
        let omit = [];
        while (!omit.length || omit[omit.length - 1] !== null) {
          omit.push(await scriptProcess(publisher, omit));
        }

        if (omit.length > 1) await browserActions.randomScroll(publisher);

        // auto-accept cookie/privacy notices
        if (opt.autolink && opt.autocontainer) {

          let click = await browserActions.clientRun(publisher, `(${clientActions.autoAccept})('${opt.autolink}','${opt.autocontainer}');`);
          if (click) {
            util.log(1, 'auto-accepted:', click);
            publisher.autoclick = click;
            await browserActions.randomScroll(publisher);
          }

        }

        // navigate to a random linked URL
        if (hop) {

          let link = await browserActions.getLinks(publisher, { inSite: true, inPage: false, inMain: true, search: false, visible: true });

          if (link.length > 5) {

            // jump to random link
            util.log(1, 'random jump within', link.length, 'page links');
            url = link[Math.floor(Math.random() * link.length)];

          }
          else {

            // stay on current page and reload if necessary
            hop = (opt.minimal ? 0 : 1);

          }

        }

      } while (hop > 0);

      // retrive advert information
      util.log(1, 'locating ads');

      let adsFound = await browserActions.clientRun(publisher, `(${clientActions.findAds})('${publisher.currentDomain}','${cfg.blacklist.join(';;')}');`);

      try {
        if (adsFound) adList = JSON.parse(adsFound);
      }
      catch(e) {
        util.log(1, 'ad err', e);
      }

      util.log(1, '\x1b[33m\x1b[1m[----- ADS:', adList.length, '-----][----- POPUPS:', publisher.popup.length, '-----]\x1b[0m');

      // no adverts to parse - finish
      if (!adList.length && !publisher.popup.length) {
        clearTimeout(timer);
        resolve({ publisher, adList });
        return;
      }

      // cancel auto-attach
      publisher.autoAttach = false;

      // sort adverts
      let adWeight = i => (i.frmD || (i.imgW * i.imgH)) / Math.max(1, i.posY / opt.device.height) + ((i.text && i.text.length) || 0);

      adList.sort((b, a) => adWeight(a) - adWeight(b));

      // limit ads to follow
      adList.length = Math.min(adList.length, opt.adfollow);

      if (!opt.minimal) {

        // find advert images
        for (let a = 0, ad; a < adList.length && (ad = adList[a]); a++) {

          // image has been set?
          if (ad.imgS) {

            if (ad.imgS.startsWith('http')) {

              // get advert image from URL
              let img = await browserActions.getImage(publisher, ad.imgS);

              if (img.imgData) {
                ad.imgData = img.imgData;
                ad.imgMime = img.imgMime;
                ad.imgExt = img.imgExt;
              }

            }
            else {

              // get advert image from data: URL
              let dc = ad.imgS.match(/^data:image\/(.+);(base64|utf8),/i);

              if (dc.length === 3) {
                ad.imgData = ad.imgS.slice(dc[0].length);
                ad.imgExt = dc[1].replace(/\+\w+$/, '').toLowerCase();
                if (ad.imgExt === 'jpeg') ad.imgExt = 'jpg';
              }

              // crop URL
              ad.imgS = ad.imgS.slice(0,60) + '...';

            }

          }
          else if (ad.fsel) {

            // ensure iframe is moved above other elements
            await browserActions.clientRun(publisher, `(${clientActions.showElement})('${ad.fsel}');`);

            // get advert frame clip from page
            let img = await browserActions.getElementImage(publisher, ad.fsel);

            // move iframe back
            await browserActions.clientRun(publisher, `(${clientActions.showElement})('${ad.fsel}');`);

            if (img) {
              ad.imgData = img;
              ad.imgExt = cfg.screenshot.type.ext;
            }

          }

          // remove blank ads
          if (ad.imgData && ad.imgExt !== 'svg' && ad.imgExt !== 'webp' && await util.imageEmpty(ad.imgData)) {

            util.log(2, 'ad', (a+1), 'blank', (ad.imgS ? ad.imgS.slice(0,50) + '...' : ''));
            ad.imgData = null;
            ad.imgExt = null;
            ad.imgMime = null;

          }

        }

      }

      // process popups
      let maxPopup = cfg.popupMax;
      for (let p = 0, popup; p < publisher.popup.length && (popup = publisher.popup[p]); p++) {

        if (maxPopup && popup.currentURL) {

          maxPopup--;

          adList.push({
            type: 'popup',
            client: popup,
            loaded: true,
            fsrc: null,
            link: popup.currentURL,
            text: null,
            imgS: null,
            imgW: 0,
            imgH: 0
          });

        }

        // close popup tab
        await browserConnect.closeTarget(popup);

      }

      // return adverts
      clearTimeout(timer);
      resolve({ publisher, adList });

    });

  }

  // public methods
  return {
    start: start
  };

};
