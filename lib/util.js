// utility functions

let logLevel = 1; // default log level

module.exports = (() => {

  'use strict';

  const
    fs = require('fs'),
    path = require('path'),
    crypto = require('crypto'),
    jimp = require('jimp');


  // set the logging level
  function setLogLevel(level = 1) {
    logLevel = level;
  }


  // log a message to the console
  let logLast;
  function log(level = 1, ...msg) {
    msg = msg.join(' ').trim();
    if (level > logLevel || !msg || msg === logLast) return;
    logLast = msg;
    if (msg.startsWith('++')) msg = `\x1b[32m  ${msg.slice(2)}\x1b[0m`;
    console.log(formatTime(), msg);
  }


  // verify a string
  function checkStr(str) {
    return String(str).trim();
  }


  // normalise a string - no non-word characters, lowercase
  function normaliseStr(str) {
    return String(str).replace(/\W/g,'').trim().toLowerCase();
  }


  // verify a URL
  function checkURL(url) {
    url = checkStr(url);
    if (url) {
      if (!url.includes('/')) url += '/';
      if (!url.startsWith('http://') && !url.startsWith('https://')) url = 'http://' + url;
    }
    return url;
  }


  // verify IP and port
  const ipMatch = /^(\d{1,3}\.){3}\d{1,3}:\d{1,5}$/;
  function verifyIP(ip) {
    ip = checkStr(ip);
    if (!ip.match(ipMatch)) ip = '';
    return ip;
  }


  // verify domain and port
  const domMatch = /^\w+([.|-]\w+){0,6}:\d{1,5}$/;
  function verifyDomain(dom) {
    dom = checkStr(dom);
    if (!dom.match(domMatch)) dom = '';
    return dom;
  }


  // verify an integer
  function checkInt(int) {
    let i = parseInt(checkStr(int), 10);
    if (!i && i !== 0) i = null;
    else i = Math.round(i);
    return i;
  }


  // split username and password string
  function splitUserPassword(str) {

    let username = '', password = '', p = String(str || '').indexOf(':');
    if (p > 0 && p < str.length - 1) {

      username = str.slice(0, p);
      password = str.slice(p+1);

    }

    return { username, password };

  }


  // extract first value from a regular expression match
  function regExract(str, re) {
    let match = String(str).match(re);
    return (match && match.length && match.length > 1 ? match[1] : '');
  }


  // token replace all { $tokens } in string
  function tokenReplace(str, token) {

    for (let t in token) {
      str = str.replace(new RegExp('\\$' + t, 'g'), token[t]);
    }

    return str;

  }


  // format date to YYYYMMDD-HHMM
  function formatDate(d = new Date()) {
    return d.getFullYear() + pad(d.getMonth()+1) + pad(d.getDate()) + '-' + pad(d.getHours()) + pad(d.getMinutes());
  }


  // format time to HH:MM:SS
  function formatTime(d = new Date()) {
    return pad(d.getHours()) + ':' + pad(d.getMinutes()) + ':' + pad(d.getSeconds());
  }


  // add leading characters to a string
  function pad(str = '', len = 2, char = '0') {
    str = String(str);
    char = String(char);
    while (str.length < len) str = char + str;
    return str;
  }


  // calculate version number
  function versionToInt(v) {
    let s = String(v).match(/\d+/gi);
    return (parseInt(s[0] || 0,10) * 1000000) + (parseInt(s[1] || 0,10) * 1000) + parseInt(s[2] || 0,10);
  }


  // extract the host domain, ignoring any subdomains to TLD
  function urlDomain(url) {

    let l;
    url = url.match(/^https{0,1}:\/\/([^/]+)\//);
    if (!url || url.length != 2 || !url[1]) return '';

    // remove last TLD
    url = url[1].split('.');
    if (url.length > 1) url.pop();

    // remove www
    if (url[0] === 'www') url.shift();

    // remove trailing TLDs
    l = url.length;
    if (l > 1 && url[l-1].length < 4 && url[l-2].length >= url[l-1].length) url.pop();

    // remove subdomains
    return url[url.length - 1].trim();

  }


  // create a regular expression which matches a url domain
  function urlDomainRegex(url, isDomain) {

    let domain = (isDomain ? url : urlDomain(url)), re = null;

    if (domain) {
      re = new RegExp('^https{0,1}:\\/\\/([^\\/]+\\.)*' + domain + '\\.', 'i');
    }

    return re;

  }


  // generate MD5 checksum
  function md5(content) {

    return crypto
      .createHash('md5')
      .update(content)
      .digest('hex');

  }


  // file access; pass mode: F_OK (visible), R_OK (readable), W_OK (writable)
  function fileAccess(file, mode) {
    try {
      fs.accessSync(file, fs[mode]);
      return true;
    }
    catch(err) {
      return false;
    }
  }


  // returns false or file lastModified date
  function fileModified(file) {

    return new Promise(resolve => {

      fs.stat(file, (err, stats) => {
        resolve(err || !stats.mtime ? false : new Date(stats.mtime));
      });

    });

  }


  // create a directory path
  function makePath(dir) {

    dir = dir.split(path.sep);
    let f, fName = '', created = true;

    for (f = 0; created && f < dir.length; f++) {

      if (dir[f]) {
        fName += (!f && dir[f].endsWith(':') ? '' : path.sep) + dir[f];
        created = fs.existsSync(fName) || typeof fs.mkdirSync(fName) === 'undefined';
      }

    }

    return created;

  }


  // returns all files in a folder modified since a date
  async function fileListUpdated(fn, date = 0) {

    let
      file = await pathRead(fn),
      match = await Promise.all(file.map(async f => {

        let fi = await pathInfo(fn + f) || {};
        fi.name = f;
        fi.mTime = fi.mTime || 0;
        fi.isFile = fi.isFile && fi.mTime > date;
        return fi;

      }));

    // remove old/invalid files
    match = match.filter(m => m.isFile);

    // sort by date order
    match.sort((a, b) => { return a.mTime - b.mTime; });

    // return names
    return match.map(m => m.name);

  }


  // is a path readable and a file or directory?
  function pathInfo(fn) {

    return new Promise(resolve => {

      // is readable
      fs.access(fn, fs.constants.R_OK, err => {

        if (err) resolve(null);
        else {

          // is file or directory?
          fs.stat(fn, (err, stats) => {

            resolve(err ? null : {
              isFile: stats.isFile(),
              isDir: stats.isDirectory(),
              mTime: stats.mtime || new Date()
            });

          });

        }

      });

    });

  }


  // fetch sub-directories and files
  function pathRead(fn) {

    return new Promise(resolve => {

      fs.readdir(fn, (err, file) => {
        resolve(err ? [] : file || []);
      });

    });

  }


  // read a file into a string
  function fileRead(fn) {

    return new Promise(resolve => {

      fs.readFile(fn, (err, data) => {
        resolve(err ? '' : data || '');
      });

    });


  }


  // file write
  function fileWrite(fn, data) {

    return new Promise(async resolve => {

      fs.writeFile(fn, data, err => {
        resolve(!!err);
      });

    });


  }


  // create a blank image
  function imageBlank(width, height, color = 0xFFFFFFFF) {

    return new Promise(resolve => {

      if (!width || !height) {
        resolve(null);
      }
      else {
        new jimp(width, height, color, (err, image) => {
          resolve((!err && image) || null);
        });
      }

    });

  }


  // is an image empty?
  // pass base64 string
  // return true if blank
  function imageEmpty(imgdata) {

    return new Promise(resolve => {

      if (!imgdata) {
        process.nextTick(() => resolve(false));
        return;
      }

      jimp.read(Buffer.from(imgdata, 'base64'), async (err, img) => {

        if (err || !img) {
          resolve('');
          return;
        }

        try {

          let
            minSize = 6,
            blank,
            w = img.bitmap.width,
            h = img.bitmap.height,
            tollerance = 0.01;

          // generate blank image
          if (!w || !h || w < minSize || h < minSize) blank = true;
          else blank = await imageBlank(w, h, img.getPixelColor(0,0));

          // result
          resolve(blank === true || (blank && jimp.diff(blank, img, tollerance).percent < tollerance));

        }
        catch (e) {
          log(2, 'image handling error', e);
          resolve(false);
        }

      });

    });

  }


  // crop an image
  // pass base64 string
  // returns base64 or empty string
  function imageCrop(imgdata, imgquality = 85) {

    return new Promise(resolve => {

      if (!imgdata) {
        process.nextTick(() => resolve(''));
        return;
      }

      jimp.read(Buffer.from(imgdata, 'base64'), async (err, img) => {

        if (err || !img) {
          resolve('');
          return;
        }

        try {

          let
            blank,
            w = img.bitmap.width,
            h = img.bitmap.height,
            ow = w,
            oh = h,
            offset = 40,
            tollerance = 0.01,
            col = [img.getPixelColor(0,0), img.getPixelColor(w-1,0), img.getPixelColor(w-1,h-1), img.getPixelColor(0,h-1)];

          img.opaque();

          // add borders using corner colours then crop
          for (let c of col) {

            img
              .background(c)
              .contain(w + offset, h)						// add width border
              .contain(w + offset, h + offset)	// add height border (done separately to ensure image is centred)
              .autocrop([tollerance, false]);

            // new width and height
            w = img.bitmap.width;
            h = img.bitmap.height;

          }

          // generate blank image
          if (!w || !h || w > ow || h > oh) blank = true;
          else blank = await imageBlank(w, h, col[0]);

          if (blank === true || (blank && jimp.diff(blank, img, tollerance).percent < tollerance)) {

            // blank image
            resolve('');

          }
          else {

            // output base64 string from buffer
            img.quality(imgquality);
            img.getBuffer(jimp.AUTO, (err, imgdata) => {
              resolve((!err && imgdata && imgdata.toString('base64')) || '');
            });

          }

        }
        catch (e) {
          log(2, 'image crop error', e);
          resolve('');
        }

      });

    });

  }


  // random characters
  const strRnd = {
    vowels: 'aeiou'.split(''),
    consts: 'bcdfghjklmnprstvwyz'.split(''),
    domain: ['random', 'aimless', 'irregular', 'driftless', 'stray', 'unaimed', 'casual', 'nameless', 'notreal', 'noemail', 'leaveme'],
    suffix: ['com', 'net', 'org']
  };


  // generate a random email address
  function emailRandom() {

    return (
      strRandom() +
      (rnd(90) + 10) +
      '@' + strRnd.domain[ rnd(strRnd.domain.length) ] + (rnd(9) + 1) +
      '.' + strRnd.suffix[ rnd(strRnd.suffix.length) ]
    );

  }


  // random seed
  var seed, rndFn = Math.random;
  function randomSeed(s) {

    if (!s) seed = 0;
    else if (!isNaN(s)) seed = Math.abs(s);
    else if (s.length) {
      seed = 0;
      let p = s.length;
      while (p > 0) {
        p--;
        seed += (p + 1) * String(s[p] || '0').charCodeAt(0);
      }
    }

    // set random generator
    rndFn = seed ? random : Math.random;

  }


  // public access to chosen random generator
  function randomizer() {
    return rndFn();
  }


  // seedable psuedo-random number generator
  function random() {

    let dp = 100000;
    if (!seed) seed = 1;
    seed = (seed * 125) % 2796203;
    return (seed % dp) / dp;

  }


  // random integer
  function rnd(max = 2) {
    return Math.floor(rndFn() * max);
  }


  // random name-like string (consonant+vowel+consonant repeated N times)
  function strRandom(len, initCap = false) {

    let str = '', set;
    len = len || rnd(2) * 3 + 3;

    while (str.length < len) {
      set = strRnd[(str.length - 1) % 3 === 0 ? 'vowels' : 'consts'];
      str += set[rnd(set.length)];
      if (initCap && str.length === 1) str = str.toUpperCase();
    }

    return str;

  }


  // web request
  function webRequest(endpoint, post) {

    return new Promise(resolve => {

      const
        url = require('url'),
        u = url.parse(endpoint);

      if (!u.protocol || !u.host) {
        error('invalid URL');
        return;
      }

      const
        web = require(u.protocol.slice(0, -1)),
        querystring = require('querystring'),
        postData = post ? querystring.stringify({ 'result': post }) : '',
        opt = {
          hostname: u.hostname,
          path: u.path,
          method: postData ? 'POST' : 'GET',
          timeout: 10000
        };

      // append post header
      if (postData) opt.headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': Buffer.byteLength(postData)
      };

      // start request
      const req = web.request(opt, (res) => {
        res.on('end', ret => resolve(ret.statusCode === 200) );
      });

      req.on('response', ret => resolve(ret.statusCode === 200) );
      req.on('timeout', e => error(e));
      req.on('error', e => error(e));

      // send data
      if (postData) req.write(postData);

      req.end();

      // request error
      function error(e) {
        log(2, url.slice(0,40), 'error', e);
        process.nextTick(() => resolve(false));
      }

    });

  }


  // public methods
  return {
    setLogLevel: setLogLevel,
    log: log,
    checkStr: checkStr,
    normaliseStr: normaliseStr,
    checkURL: checkURL,
    verifyIP: verifyIP,
    verifyDomain: verifyDomain,
    checkInt: checkInt,
    splitUserPassword: splitUserPassword,
    regExract: regExract,
    tokenReplace: tokenReplace,
    formatDate: formatDate,
    formatTime: formatTime,
    pad: pad,
    versionToInt: versionToInt,
    urlDomain: urlDomain,
    urlDomainRegex: urlDomainRegex,
    md5: md5,
    fileAccess: fileAccess,
    fileModified: fileModified,
    fileListUpdated: fileListUpdated,
    pathInfo: pathInfo,
    pathRead: pathRead,
    fileRead: fileRead,
    fileWrite: fileWrite,
    makePath: makePath,
    imageEmpty: imageEmpty,
    imageCrop: imageCrop,
    randomSeed: randomSeed,
    random: random,
    randomizer: randomizer,
    strRandom: strRandom,
    emailRandom: emailRandom,
    webRequest: webRequest
  };

})();
