// connect to browser instance

// static properties
const
  CDP = require('chrome-remote-interface'),
  browserSetup = require('./browser-setup'),
  util = require('./util'),
  client = {
    main: null,
    port: null
  };

module.exports = opt => {

  'use strict';

  // set options
  client.port = opt.port || null;
  util.setLogLevel(opt.verbose);


  // find list of targets
  async function listTargets() {

    if (!client.main) {
      return await cdpList();
    }
    else {
      let targets = await client.main.Target.getTargets();
      return (targets && targets.targetInfos) || [];
    }

  }


  // find list of targets using CDP
  function cdpList() {

    return new Promise(resolve => {

      CDP.List(
        { port: client.port },
        (err, targets) => {
          resolve (!err && targets ? targets : []);
        }
      );

    });

  }


  // find a target by ID
  async function findTarget(targetId) {

    // get current list of targets
    let targets = await listTargets();

    // attach to matching target
    return targets.find(e => targetId === e.targetId || targetId === e.id);

  }


  // connect to initial browser tab
  async function mainTarget() {

    // attach to new target, set as main
    let cli = await newTarget(true);

    // close all other tabs
    if (cli) {
      client.main = cli;
      await closeAll(cli.targetId);
    }

    return cli;

  }


  // new tab connection
  function newTarget(isMain, retry = 5, delay = 1000) {

    return new Promise(async resolve => {

      attempt();
      function attempt() {

        CDP.New(
          { port: client.port },
          async (err, target) => {

            if (!err && target) {

              let cli = await attachToTarget(target.webSocketDebuggerUrl, target.id, !!isMain);
              if (cli) util.log(2, 'new target opened', target.id);
              resolve(cli);

            }
            else {

              // retry
              retry--;
              if (retry > 0) {
                setTimeout(attempt, delay);
              }
              else {
                util.log(1, 'no targets', err || '');
                resolve(null);
              }

            }

          }
        );

      }

    });


  }


  // connect to a target by ID
  async function connectTarget(targetId) {

    // get current list of targets
    let
      cli = null,
      t = await findTarget(targetId);

    // attach to matching target
    if (t) {
      cli = await attachToTarget(t.webSocketDebuggerUrl || null, targetId);
    }

    return cli;

  }


  // attach to a target
  function attachToTarget(wsUrl, targetId, isMain) {

    let connect = {
      port: client.port,
      target: wsUrl || targetId
    };

    return new Promise(resolve => {

      CDP(
        connect,
        async cli => {

          cli.targetId = targetId;
          cli.isMain = !!isMain;
          await browserSetup(cli, opt);
          resolve(cli);

        }
      ).on('error', () => {
        resolve(null);
      });

    });

  }


  // close target
  async function closeTarget(cli) {

    // can close?
    if (!client.main || !cli || !cli.targetId) return;

    let close = await client.main.Target.closeTarget({ targetId: cli.targetId });
    util.log(2, 'target', cli.targetId, (close.success ? '' : 'NOT ') + 'closed');

  }


  // close all targets with optional target ID exception
  async function closeAll(except) {

    let targets = await listTargets();

    // close targets
    for (let t = 0; targets && t < targets.length; t++) {

      if (!except || targets[t].targetId !== except) {
        await closeTarget({ targetId: targets[t].targetId });
      }

    }

  }


  // public methods
  return {
    listTargets: listTargets,
    findTarget: findTarget,
    mainTarget: mainTarget,
    newTarget: newTarget,
    connectTarget: connectTarget,
    closeTarget: closeTarget,
    closeAll: closeAll
  };

};
