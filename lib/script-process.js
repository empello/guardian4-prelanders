// process scripts

// static properties
const util = require('./util');

module.exports = (opt, type) => {

  'use strict';

  // set options
  util.setLogLevel(opt.verbose);

  const
    execPause = 5000, // delay after CLICK or RUN
    script = opt[type],
    browserActions = require('./browser-actions')(opt),
    clientScript = require('./client-script');


  // start script processing
  return async function(tab, omit = [], screenshot = false) {

    let
      found = false,
      mainFrame = true,
      active, cmd;

    // script found?
    await findScript();
    if (found) {

      util.log(1, 'active', type, script[active].file);

      // run remaining commands
      let ret = true;
      while (ret) {
        ret = await runCommand();
      }

      await browserActions.pause(execPause);
      util.log(2, type, 'processing complete');

    }

    // final screenshot
    await logShot();

    return found ? active : null;

    // ________________________________________________________________________
    // find a suitable script
    async function findScript() {

      let pl = 0;
      while (script && !found && pl < script.length) {

        active = pl;
        cmd = 0;

        let
          exec = true,
          next = nextCommand();

        // inital match of PROXY, LAND and HAS
        while (exec && next && (next.cmd === 'PROXY' || next.cmd === 'LAND' || next.cmd === 'HAS')) {
          exec = await runCommand();
          next = nextCommand();
        }

        // script match?
        if (exec && next && !omit.includes(pl)) found = true;
        else pl++;

      }

      return found;

    }

    // ________________________________________________________________________
    // get next command
    function nextCommand() {
      return (script[active] && cmd <= script[active].script.length ? script[active].script[cmd] : false);
    }

    // ________________________________________________________________________
    // run next script command
    async function runCommand() {

      let
        ret = false,
        next = nextCommand();

      if (!next) return ret;

      await tab.Page.bringToFront();

      switch (next.cmd) {

        case 'PROXY':
          ret = !!(opt.proxy || 'local').match(new RegExp(next.arg, 'i'));
          break;

        case 'LAND':
          ret = !!tab.currentURL.match(new RegExp(next.arg, 'i'));
          break;

        case 'HAS':
          ret = !!(await findNode(next.arg));
          break;

        case 'FRAME':
          ret = await setFrame(next.arg);
          if (ret) mainFrame = (next.arg === '#');
          break;

        case 'FIELD':
          ret = await setField(next.arg[0], next.arg[1]);
          break;

        case 'CHECK':
          ret = await setField(next.arg[0], next.arg[1]);
          break;

        case 'TOUCH':
          ret = await doClick(next.arg);
          break;

        case 'CLICK':
          await logShot();
          ret = await doClick(next.arg);
          break;

        case 'RUN':
          await logShot();
          await browserActions.clientRun(tab, next.arg);
          await browserActions.pause(execPause);
          ret = true;
          break;

        case 'SHOT':
          await logShot();
          ret = true;
          break;

        case 'WAIT':
          await browserActions.pause(next.arg);
          ret = true;
          break;

      }

      if (found) util.log((opt.test ? 1 : 1), 'EXEC ', next.cmd, String(next.arg).slice(0,40) + '...', '->', (ret ? 'done' : 'FAILED'));

      // next command
      if (ret) cmd++;

      return ret;

    }


    // ________________________________________________________________________
    // log a new screenshot
    async function logShot() {

      if (screenshot) await browserActions.screenshotLog(tab);

    }


    // ________________________________________________________________________
    // inject client script code
    async function injectScript() {

      let injected = await browserActions.clientRun(tab, '!!(window.guardian4 && window.guardian4.script);');

      if (!injected) {
        await browserActions.clientRun(tab, `window.guardian4 = window.guardian4 || {}; window.guardian4.script = (${clientScript})();`);
      }

    }


    // ________________________________________________________________________
    // matching text or selector in page
    async function findNode(item) {

      if (!item) return false;
      await injectScript();

      item = item.replace(/'/g,'\\\'');
      return await browserActions.clientRun(tab, `window.guardian4.script.find('${item}');`);

    }


    // ________________________________________________________________________
    // navigate to frame
    async function setFrame(item) {

      if (!item) return false;
      await injectScript();

      item = item.replace(/'/g,'\\\'');
      return await browserActions.clientRun(tab, `window.guardian4.script.setFrame('${item}');`);

    }


    // ________________________________________________________________________
    // set a form value
    async function setField(item, value) {

      if (!item) return false;
      await injectScript();

      item = String(item).replace(/'/g,'\\\'');
      value = util.tokenReplace(String(value), opt.user).replace(/'/g,'\\\'');

      return await browserActions.clientRun(tab, `window.guardian4.script.setField('${item}','${value}');`);

    }


    // ________________________________________________________________________
    // tap a button
    async function doClick(item) {

      if (!item) return false;
      await injectScript();

      item = item.replace(/'/g,'\\\'');

      // synthetic click in child frame
      if (!mainFrame) {
        let click = await browserActions.clientRun(tab, `window.guardian4.script.doClick('${item}');`);
        if (click) await browserActions.pause(execPause);
        return click;
      }

      // emulate a real tap event (main frame only)
      let element = await browserActions.clientRun(tab, `window.guardian4.script.find('${item}');`);
      if (element) {
        return await browserActions.tapElement(tab, element);
      }
      else return false;

    }


  };

};
