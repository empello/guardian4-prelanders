// write log

module.exports = (async (opt, publisher, adList) => {

  'use strict';

  return new Promise(async resolve => {

    const
      cfg = require('../config.json'),
      util = require('./util'),
      path = require('path'),
      fs = require('fs'),
      url = require('url'),
      shotRe = /(\[SHOT\s*:\s+)(\d+)(.*)/i,
      logfile =
        util.formatDate() + '-' +
        (publisher.currentURL || '')
          .replace(/http[s]*:\/*(w+\.)*/i,'')
          .replace(/\/.+$/,'')
          .replace(/[^a-z0-9.]/ig,'')
          .replace(/-+/g,'-')
          .replace(/[-]+$/,'')
          .toLowerCase() + '-' +
        opt.deviceCode + '-' +
        (opt.country ? opt.country + '-' : (opt.proxy ? opt.proxy.replace(/^.+:/,'') + '-' : ''));

    util.setLogLevel(opt.verbose);
    util.log(1, 'writing logs');

    let
      adId = 0,
      result = null,
      fail = '',
      pubFile = publisher.cacheFile,
      timer = setTimeout(() => {
        // 10 second timeout
        resolve({ adId, result });
      }, 10000);

    // publisher screenshot
    if (pubFile && cfg.screenshot.capture && !publisher.saveBandwidth && !publisher.hijack) {

      let pubsc = await require('./browser-actions')(opt).screenshot(publisher, cfg.screenshot.publisherFull);

      // cache image
      if (pubsc && publisher.cacheCreate) {
        await writeFile(pubFile, pubsc, 'base64');
      }

    }

    // get cached publisher file
    if (opt.minimal || !pubFile ||!util.fileAccess(pubFile, 'R_OK')) pubFile = '';

    // result container
    result = {
      ip:       publisher.ipAddress || '',
      url:      publisher.currentURL || '',
      agree:    publisher.autoclick || '',
      complete: new Date(),
      ad:       []
    };


    // scrape publisher page
    if (opt.scrape && pubFile) {

      // copy publisher screenshot
      fs.copyFileSync(pubFile, opt.logfolder + logfile + '00.' + cfg.screenshot.type.ext);

      // log file
      let log = [];
      if (publisher.ipAddress) log.push(`\n[IP   : ${publisher.ipAddress} ]`);
      if (publisher.autoclick) log.push(`\n[AGREE: ${publisher.autoclick} ]`);
      await writeFile('00urls.log', publisher.log.concat(log).join('\n'));

      // html and text
      if (publisher.html && publisher.html.length >= cfg.minContent) {

        let
          content = HTMLtoPlain(publisher.html),
          html = publisher.html.slice(0, cfg.textCrop);

        await writeFile('00pub.html', html);

        if (content) {
          await writeFile('00pub.txt', content);
        }

      }

    }


    // examine all journeys
    for (let a = 0, ad; a < adList.length && (ad = adList[a]); a++) {

      let
        log = [],
        hist = [],
        aRes = {
          text: '',
          imgUrl: '',
          linkUrl: '',
          view: {},
          scrPub: '',
          scrAd: '',
          landUrl: '',
          log: '',
          html: '',
          plain: '',
          journey: []
        };

      // dimensions
      ad.posX = Math.ceil(ad.posX || 0);
      ad.posY = Math.ceil(ad.posY || 0);
      ad.width = Math.ceil(ad.imgW || ad.frmW || 0);
      ad.height = Math.ceil(ad.imgH || ad.frmH || 0);

      if (ad.posX && ad.posY && ad.width && ad.height) {
        ad.inview = Math.round(Math.max(0, Math.min((opt.device.height - ad.posY) / ad.height,1)) * 100);
        ad.pagedn = ad.inview === 100 ? 1 : Math.round(ad.posY / opt.device.height) + 1;
      }

      // initial log file
      if (publisher.ipAddress) log.push(`\n[IP   : ${publisher.ipAddress} ]`);

      if (publisher.autoclick) log.push(`\n[AGREE: ${publisher.autoclick} ]`);

      log.push(`\n[TYPE : ${ad.type} ]`);
      if (ad.text) {
        log.push(`[TEXT : ${ad.text} ]`);
        aRes.text = ad.text;
      }
      if (ad.imgS) {
        log.push(`[ADIMG: ${ad.imgS} ]`);
        aRes.imgUrl = ad.imgS;
      }
      if (ad.link) {
        if (ad.link.startsWith('javascript:')) log.push(`[JSRUN: ${ad.link} ]`);
        else {
          if (opt.nofollow) log.push(`[LINK : ${ad.link} ]`);
          aRes.linkUrl = ad.link;
        }
      }

      if (ad.pagedn) {

        log.push(`[VIEW : x ${ad.posX}, y ${ad.posY}, width ${ad.width}, height ${ad.height}, view: ${ad.inview}%, page: ${ad.pagedn} ]`);

        aRes.view = {
          x: ad.posX,
          y: ad.posY,
          width: ad.width,
          height: ad.height,
          view: ad.inview,
          page: ad.pagedn
        };

      }

      if (ad.type === 'popup') {
        log.push('\nPOPUP');
      }
      else if (ad.type === 'hijack') {
        log.push('\nHIJACK', publisher.hijack || []);
      }
      else if (!opt.nofollow) {
        log.push('\nADVERT CLICKED');
      }

      // history
      if (ad.history && ad.history.length) {
        hist.push('\nBACK-BUTTON HIJACKING');
        ad.history.forEach(h => {
          hist.push('[TITLE: ' + h.title + ']', h.url);
        });
      }

      if (ad.loaded || opt.nofollow) {

        // journey result available
        adId++;
        let
          ic = util.pad(adId),
          ll = ad.log && ad.log.length;

        for (let l = 0; l < ll && !opt.nofollow; l++) {

          // add redirect to log
          log.push(ad.log[l]);

          // add to journey
          let jN = aRes.journey.length;
          aRes.journey[jN] = {
            dom: url.parse(ad.log[l]).hostname || '',
            url: ad.log[l],
            scr: []
          };

          // screenshots
          if (ad.screenURL && ad.screenURL[l]) {

            ad.screenURL[l] = ad.screenURL[l].map(s => {

              // convert [SHOT : n ] lines
              let m = s.match(shotRe);
              if (m && m.length === 4) {
                let fn = logfile + ic + 'land' + util.pad(m[2]) + '.' + cfg.screenshot.type.ext;
                s = m[1] + fn + m[3];
                aRes.journey[jN].scr.push(fn);
              }
              return s;

            });

            log = log.concat(ad.screenURL[l]);

          }

        }

        // landing page
        if (ll && !opt.nofollow) {
          log.push('\nLANDING PAGE');
          aRes.landUrl = ad.log[ad.log.length - 1];
          log.push(aRes.landUrl);

          // title
          if (ad.title) {
            ad.title = ad.title.replace(/\s+/g, ' ').trim();
            if (ad.title.length) log.push('[TITLE: ' + ad.title + ' ]');
          }

          // final screenshot
          if (ad.screenURL && ad.screenURL[ll-1]) {
            log.push(ad.screenURL[ll-1].pop());
          }
        }

        // masking
        if (ad.mask) {
          log = log.concat('\nCONTENT MASKING', ad.mask.trimRight());
        }

        // forced downloads
        if (ad.download && ad.download.length) {
          let dl = ad.download.filter(url => !ad.log.includes(url));
          if (dl.length) log = log.concat('\nADDITIONAL URLS/TRACKERS', dl);
        }

        // downloaded files
        if (ad.downloadFiles && ad.downloadFiles.length) {
          log = log.concat('\nFORCED DOWNLOAD FILES', ad.downloadFiles);
        }

        // download links
        if (ad.downloadLink) {
          log = log.concat('\nSUSPICIOUS DOWNLOAD LINKS', ad.downloadLink.trimRight());
        }

        // history
        log = log.concat(hist);

        // copy publisher screenshot
        if (pubFile) {
          aRes.scrPub = logfile + ic + '.' + cfg.screenshot.type.ext;
          fs.copyFileSync(pubFile, opt.logfolder + aRes.scrPub);
        }

        // advert image
        if (ad.imgData) {
          let f = ic + 'ad.' + ad.imgExt;
          await writeFile(f, ad.imgData, 'base64');
          aRes.scrAd = logfile + f;
        }

        // log
        await writeFile(ic + 'urls.log', publisher.log.concat(log).join('\n'));
        aRes.log = logfile + ic + 'urls.log';

        // html
        if (ad.html && ad.html.length >= cfg.minContent) {
          ad.content = HTMLtoPlain(ad.html);
          ad.html = ad.html.slice(0, cfg.textCrop);
          let f = ic + 'land.html';
          await writeFile(f, ad.html);
          aRes.html = logfile + f;
        }

        // text content
        if (ad.content) {

          let f = ic + 'land.txt';
          await writeFile(f, ad.content);
          aRes.plain = logfile + f;

        }

        // landing page screenshots
        if (ad.screen && Array.isArray(ad.screen)) {

          for (let s = 0; s < ad.screen.length; s++) {
            await writeFile(ic + 'land' + util.pad(s+1) + '.' + cfg.screenshot.type.ext, ad.screen[s], 'base64');
          }

        }

        // append to result
        result.ad.push(aRes);

      }
      else if (!ad.blacklisted) {

        // failed journey log
        if (!fail) {
          fail = 'FAILED JOURNEYS\n\n' + publisher.log.join('\n');
        }

        if (!ad.log || !ad.log.length) {
          ad.log = ['load failed to initiate:', ad.link];
        }

        log = log.concat(ad.log, hist);
        fail += '\n\n' + '_'.repeat(60) + '\n' + log.join('\n');

      }

    }

    // output failures
    if (fail) await writeFile('fail.log', fail);

    clearTimeout(timer);
    resolve({ adId, result });


    // HTML to plain text
    function HTMLtoPlain(text) {

      return text
        .replace(/<!--[\S\s]*?-->/g, ' ')             // remove comments
        .replace(/<script[\S\s]*?<\/script>/ig, ' ')  // remove inline scripts
        .replace(/<style[\S\s]*?<\/style>/ig, ' ')    // remove inline styles
        .replace(/\s+/g, ' ')                         // remove spacing
        .replace(/<\/(title|main|article|section|div|h1|h2|h3|h4|h5|h6|blockquote)>/ig, '\n\n')
        .replace(/<\/(p|li|tr)>/ig, '\n')
        .replace(/<br\s*\/*>/ig, '\n')
        .replace(/<[\S\s]*?>/g, ' ')                  // remove tags
        .replace(/&amp;/g, '&')                       // replace entities
        .replace(/&lt;/g, '<')
        .replace(/&laquo;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&raquo;/g, '>')
        .replace(/&quot;/g, '"')
        .replace(/&hellip;/g, '...')
        .replace(/&nbsp;/g, ' ')
        .replace(/&pound;/g, '£')
        .replace(/&\w+;/g, ' ')
        .replace(/[ ]+/g, ' ')                        // remove excess space
        .replace(/[ ]+(\.|,|;|:)/g, '$1')
        .replace(/[ ]+\n/g, '\n')                     // trim lines
        .replace(/\n[ ]+/g, '\n')
        .replace(/\n[ .,;:@#-+|]+\n/g, '\n')
        .replace(/\n{3,}/g, '\n\n')
        .trim()
        .slice(0, cfg.textCrop);

    }


    // write a log file
    function writeFile(filename = 'err.txt', data = '', enc = 'utf8') {

      return new Promise(async resolve => {

        // set full filename
        if (!filename.includes(path.sep)) filename = opt.logfolder + logfile + filename;

        // write file
        fs.writeFile(filename, data, enc, err => {
          resolve(!!err);
        });

      });

    }

  });

});
