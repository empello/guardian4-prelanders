// broswser actions

// static properties
const
  cfg = require('../config.json'),
  util = require('./util');

module.exports = opt => {

  'use strict';

  util.setLogLevel(opt.verbose);

  /*
    These don't appear to be usable.
    The DOM document and nodeIds change even when no action has occurred?
  */

  // find single element
  async function querySelector(client, selector) {

    let
      DOM = client.DOM,
      doc = await DOM.getDocument(),
      node = await DOM.querySelector({
        nodeId: doc.root.nodeId,
        selector: selector
      });

    return (node && node.nodeId) || null;

  }


  // find all elements
  async function querySelectorAll(client, selector) {

    let
      DOM = client.DOM,
      doc = await DOM.getDocument(),
      node = await DOM.querySelectorAll({
        nodeId: doc.root.nodeId,
        selector: selector
      });

    return (node && node.nodeIds) || [];

  }


  // CLIENT: fetch content of selector
  function clientContent(selector) {
    var t = document.querySelector(selector);
    return (t && t.textContent) || '';
  }


  // get content of selector
  async function getContent(client, selector) {
    return (await clientRun(client, `(${clientContent})('${selector}');`) || '').trim();
  }


  // CLIENT: get array of page links
  // cfg.visible = true for visible, false for hidden
  // cfg.inSite = true for within site, false for off-site
  // cfg.inMain = true to locate links within main content (not header, footer, etc)
  // cfg.inPage = true for within page, false for different page
  // cfg.search = true for ?args, false for no ?args
  function clientGetLinks(cfg) {

    cfg = cfg || {};

    let
      host = window.location.host,
      pathname = window.location.pathname,
      link = Array.from(document.getElementsByTagName('a')),
      url = [], p, u;

    if (typeof cfg.inMain === 'boolean' && cfg.inMain) {

      // remove links in header, footer and navigation
      let remLink = Array.from(document.querySelectorAll('header a, #header a, .header a, #head a, .head a, footer a, #footer a, .footer a, #foot a, .foot a, nav a, #nav a, .nav a, #menu a, .menu a, #hero a, .hero a'));

      remLink.forEach(a => {
        let i = link.indexOf(a);
        if (i >= 0) link[i] = null;
      });

    }

    for (p = 0; p < link.length; p++) {

      u = link[p];
      if (
        u && u.host && u.pathname &&
        !url.includes(u.href) &&
        ( typeof cfg.inSite !== 'boolean' || cfg.inSite === (u.host === host) ) &&
        ( typeof cfg.inPage !== 'boolean' || cfg.inPage === (u.pathname === pathname) ) &&
        ( typeof cfg.search !== 'boolean' || cfg.search === !!u.search ) &&
        ( typeof cfg.visible !== 'boolean' || cfg.visible === !!u.offsetHeight )
      ) {
        url.push(u.href);
      }

    }

    return JSON.stringify(url);

  }


  // get links within the page
  async function getLinks(client, cfg) {

    let
      ret = [],
      url = await clientRun(client, `(${clientGetLinks})(${JSON.stringify(cfg)});`);

    try {
      ret = JSON.parse(url);
    }
    catch(e) {
      util.log(2, 'link array error', e);
    }

    return ret;
  }


  // get flattened document (all nodes by default)
  async function documentNodes(client, depth = -1) {

    let dom = await client.DOM.getFlattenedDocument({ depth });
    return (dom && dom.nodes) || [];

  }


  // run JS in client
  async function clientRun(client, expression = 'false') {

    let
      exec = (client && client.Runtime ? await client.Runtime.evaluate({ expression }) : false),
      ret = (exec.result && exec.result.value);

    return typeof ret === 'undefined' ? false : ret;

  }


  // set additonal headers
  async function setHeaders(client, headers = {}) {

    // set default language
    headers['Accept-Language'] = headers['Accept-Language'] || opt.language;

    await client.Network.setExtraHTTPHeaders({ headers });

  }


  // set browser emulation metrics
  async function setDeviceSize(client, page = {}) {

    let
      width = page.width || opt.device.width,
      height = page.height || opt.device.height,
      posX = page.posX || 0,
      posY = page.posY || 0;

    await client.Emulation.setDeviceMetricsOverride({
      width: width,
      screenWidth: width,
      height: height,
      screenHeight: height,
      positionX: posX,
      positionY: posY,
      scale: 1,
      deviceScaleFactor: 0,
      mobile: true,
      fitWindow: true,
      screenOrientation: {
        type: 'portraitPrimary',
        angle: 32
      }
    });

    await client.Emulation.setVisibleSize({
      width: width,
      height: height
    });

    await client.Emulation.setDefaultBackgroundColorOverride({
      color: { r: 255, g: 255, b: 255, a: 1 }
    });

    await client.Emulation.setTouchEmulationEnabled({
      enabled: true
    });

  }


  // clear all cookies
  async function clearCookies(client) {

    let clear = await client.Network.canClearBrowserCache();
    if (clear && clear.result) {
      await client.Network.clearBrowserCache();
      util.log(2, 'browser cookies cleared');
    }

  }


  // load a URL
  function loadURL(client, url) {

    return new Promise(async resolve => {

      let
        startLoad = +new Date(),
        loadTimer = setInterval(async () => {

          let
            now = +new Date(),
            timeResp = now - client.lastReceive,
            timeTotal = now - startLoad;

          if (!client.loaded && timeTotal > cfg.timeout.triggerStop) {
            util.log(2, 'trigger stop', url.slice(0,30), (client && client.currentURL && client.currentURL.slice(0,30)) || '');
            await client.Page.stopLoading();
          }

          if (
            (timeResp > cfg.timeout.maxResponse) || // no recent data load
            (timeTotal > cfg.timeout.loadPage) // maximum page load
          ) {
            util.log(2, 'page load timeout', url.slice(0,30), (client && client.currentURL && client.currentURL.slice(0,30)) || '');
            await loadEvent();
          }

        }, 2000);

      // wait for load event
      client.event.once('loaded', async () => { await loadEvent(); });

      // load URL
      if (url) {
        await client.Page.bringToFront();
        await client.Page.navigate({ url });
      }
      else if (client.loaded) await loadEvent();

      // complete loading
      async function loadEvent() {
        await client.Page.bringToFront();
        clearInterval(loadTimer);
        client.event.removeListener('loaded', loadEvent);
        await getHTML(client);
        await resolve(client.loaded);
      }

    });

  }


  // run script which could load a page
  function runScript(client, expression) {

    return new Promise(async resolve => {

      if (!expression) resolve(client.loaded);

      await client.Page.bringToFront();

      // page load timeout
      let timeout = setTimeout(() => {
        util.log(2, 'page load timeout', (client && client.currentURL && client.currentURL.slice(0,30)) || '');
        loadEvent();
      }, cfg.timeout.loadPage);

      // wait for load event
      client.event.on('loaded', loadEvent);

      // evaluate - allowing window.open
      await clientRun(client, `window.guardian4.winOpenAllow=true;${expression};window.guardian4.winOpenAllow=false;`);

      // complete loading
      function loadEvent() {
        client.event.removeListener('loaded', loadEvent);
        if (timeout) clearTimeout(timeout);
        resolve(client.loaded);
      }

    });

  }


  // tap/click an element and wait for new page load (if required)
  function tapElement(client, selector, loadWait = true) {

    return new Promise(async resolve => {

      // scroll element to bottom of viewport
      let found = await scrollToElement(client, selector, 3);
      if (found === false) resolve(false);

      // get box
      let box = await elementMetrics(client, selector);
      if (!box || !box.content) resolve(false);

      // calculate click position
      let
        p = box.content,
        w = (p[2] - p[0]),
        h = (p[7] - p[1]),
        x = Math.round(p[0] + (w * 0.2) + (w * 0.6 * Math.random())),
        y = Math.round(p[1] + (h * 0.2) + (h * 0.6 * Math.random())),
        timeoutRequest, timeoutLoad;

      if (loadWait) {

        // page request made?
        timeoutRequest = setTimeout(() => {
          if (client.loaded) loadEvent();
        }, cfg.timeout.firstRequest);

        // page load timeout (will come after timeoutRequest)
        timeoutLoad = setTimeout(() => {
          util.log(2, 'page load timeout', (client && client.currentURL && client.currentURL.slice(0,30)) || '');
          loadEvent();
        }, Math.max(cfg.timeout.firstRequest, cfg.timeout.loadPage) + 10);

        // wait for load event
        client.event.once('loaded', loadEvent);

      }

      try {
        // tap element
        await client.Page.bringToFront();
        await client.Input.synthesizeTapGesture({ x, y });
      }
      catch(e) {
        util.log(2, 'tap error at', x, ',', y, ':', e);
      }

      if (!loadWait) resolve(true);

      // complete loading
      function loadEvent() {
        client.event.removeListener('loaded', loadEvent);
        if (timeoutRequest) clearTimeout(timeoutRequest);
        if (timeoutLoad) clearTimeout(timeoutLoad);
        resolve(client.loaded);
      }

    });

  }


  // returns true if a page could redirect
  let reMeta = /(\d)\s*;\s*url\s*=\s*['|"]{0,1}([^'|"|\s]+)/i;
  function couldRedirect(client) {

    let
      refresh = { time: 0, url: null },
      html = client.html;

    // look for N;URL meta redirect
    if (html && !html.includes('<script')) {
      let meta = html.match(reMeta);
      if (meta && meta.length === 3 && meta[1] && meta[2]) {
        refresh.time = parseInt(meta[1], 10);
        refresh.url = meta[2];
      }
    }

    return refresh;

  }


  // random scroll down and back to top to emulate user actions
  async function randomScroll(client) {

    // ensure scroll is possible
    let
      yDist = 0, maxAttempt = 5,
      page = await pageMetrics(client),
      pHeight = (page && page.contentSize ? page.contentSize.height : 0);

    if (pHeight) yDist = Math.floor(Math.min(pHeight - page.layoutViewport.clientHeight, page.layoutViewport.clientHeight * (1 + Math.random())));

    await pause();
    if (!yDist) return;

    // scroll down
    yDist = await scroll(client, -yDist);
    await pause();

    // scroll to top
    while (maxAttempt && yDist * page.visualViewport.scale > 4) {
      yDist = await scroll(client, yDist);
      await pause(200 * Math.random());
      maxAttempt--;
    }

  }


  // scroll (minus yDist for down, positive yDist for up)
  async function scroll(client, yDist = -Math.round(Math.random() * (opt.device.height - 300) + 300)) {

    await client.Page.bringToFront();

    let page = await pageMetrics(client);
    if (!page || !page.contentSize.height) return 0;

    let yPos = page.layoutViewport.pageY;

    if (yDist < 0) {
      // scroll down
      let sMax = page.contentSize.height - page.layoutViewport.clientHeight - yPos;
      yDist = Math.max(yDist, -sMax);
    }
    else {
      // scroll up
      yDist = Math.min(yDist, yPos);
    }

    yDist = Math.round(yDist);
    if (!yDist) return page.layoutViewport.pageY;

    if (Math.abs(yDist) > 10) util.log(2, 'scrolling', yDist);

    let
      dw = page.layoutViewport.clientWidth,
      ow = 0.5, // horizontal area - anywhere in middle 50%
      dh = page.layoutViewport.clientHeight,
      oh = Math.round(Math.random() * dh * 0.2); // vertical area - anywhere in top/bottom 20%

    /*
      to use overscrolling:
      let yOver = Math.sign(yDist) * Math.max(0, Math.abs(yDist) - page.layoutViewport.clientHeight - oh);
      yDist -= yOver;

      add yOverscroll: yOver to synthesizeScrollGesture
    */

    try {

      await client.Input.synthesizeScrollGesture({
        x: Math.round((Math.random() * dw * ow) + (dw * ow / 2)),
        y: (yDist < 0 ? oh : dh - oh),
        xDistance: Math.round(Math.random() * 40 - 20),
        yDistance: yDist
      });

    }
    catch(e) {
      util.log(2, 'scroll error:', yDist, e);
    }

    // return final position of page
    page = await pageMetrics(client);
    return (page && page.layoutViewport ? page.layoutViewport.pageY : 0);

  }


  // scroll to a specific pixel
  async function scrollTo(client, yPos) {

    let page = await pageMetrics(client);

    if (!page || !page.contentSize.height) return 0;

    // scroll to position
    await scroll(client, page.layoutViewport.pageY - yPos);

    // return new y-position
    page = await pageMetrics(client);
    return (page && page.layoutViewport ? page.layoutViewport.pageY : 0);

  }


  // scroll an element into view
  // mode 1 = top, 2 = middle, 3 = bottom
  async function scrollToElement(client, selector, mode = 2) {

    // get metrics
    let
      page = await pageMetrics(client),
      box = await elementMetrics(client, selector);

    if (!page || !page.contentSize.height || !box) return false;

    let
      vh = page.layoutViewport.clientHeight,
      y1 = box.border[1], y2 = box.border[7],
      yPos;

    switch (mode) {
      case 1:
        // top
        yPos = y1;
        break;

      case 3:
        // bottom
        yPos = y2 - vh;
        break;

      default:
        // middle
        yPos = y1 - (vh / 2) + ((y2 - y1) / 2);
        break;
    }

    // scroll into view
    return await scrollTo(client, yPos);

  }


  // get page metrics
  async function pageMetrics(client) {

    await client.Page.bringToFront();

    let page = await client.Page.getLayoutMetrics();
    return (!page || !page.contentSize ? null : page);

  }


  // get element metrics
  async function elementMetrics(client, selector) {

    await client.Page.bringToFront();

    let
      DOM = client.DOM,
      doc = await DOM.getDocument(),
      node = await DOM.querySelector({
        nodeId: doc.root.nodeId,
        selector: selector
      });

    if (!node || !node.nodeId) return null;

    // element box
    try {
      let box = await DOM.getBoxModel({ nodeId: node.nodeId });
      return (!box || !box.model || !box.model.content || box.model.content.length !== 8 ? null : box.model);
    }
    catch(e) {
      return null;
    }

  }


  // pause for a period of delay ms
  function pause(delay = Math.random() * 1000 + 500) {

    if (delay > 500) util.log(2, 'pausing', Math.ceil(delay / 1000), 'seconds');

    return new Promise(resolve => {
      setTimeout(() => { resolve(true); }, delay);
    });

  }


  // get screenshot data
  function screenshot(client, fullpage) {

    return new Promise(async resolve => {

      if (!client.loaded || !cfg.screenshot.capture || opt.minimal) {
        resolve('');
        return;
      }

      let height = opt.device.height;

      await client.Page.bringToFront();

      // full page capture
      if (fullpage) {

        // get page metrics
        let page = await pageMetrics(client);
        height = Math.ceil(((page && page.contentSize && page.contentSize.height) || 0) * ((page && page.visualViewport && page.visualViewport.scale) || 1));

        if (height <= opt.device.height) {

          // retain browser height
          height = opt.device.height;
          fullpage = false;

        }
        else {

          // set new height
          height = Math.min(height, cfg.screenshot.maxHeight);
          await setDeviceSize(client, { height });

        }

      }

      // capture and auto-crop screen
      let
        screen = null,
        timeout = setTimeout(async function() { await end(); }, 5000);

      screen = await client.Page.captureScreenshot(cfg.screenshot.type);
      screen = screen && screen.data;

      // crop image smaller than cfg.screenshot.cropLimit pixels
      if (screen && opt.device.width * height <= cfg.screenshot.cropLimit) {

        let cropped = await util.imageCrop(screen, cfg.screenshot.type.quality);
        if (timeout && cropped) screen = cropped;

      }

      await end();

      // tidy up
      async function end() {

        clearTimeout(timeout);

        // reset page size
        if (fullpage) await setDeviceSize(client);

        resolve(screen);

      }

    });

  }


  // capture a screenshot to client screen and log arrays
  async function screenshotLog(client) {

    client.screen = client.screen || [];
    client.screenURL = client.screenURL || [];

    let
      img = await screenshot(client, cfg.screenshot.landingFull),
      cs = client.screen.length;

    if (img && (!cs || img !== client.screen[cs-1])) {

      client.screen.push(img);

      // add to screen URL log
      if (client.log) {
        let lp = client.log.length - 1;
        client.screenURL[lp] = client.screenURL[lp] || [];
        client.screenURL[lp].push(`[SHOT : ${cs+1} ]`);
      }
    }

  }


  // fetch page HTML
  async function getHTML(client) {

    if (client && client.htmlRequestId) {

      try {
        let
          oldHtml = client.html,
          html = await client.Network.getResponseBody({ requestId: client.htmlRequestId });

        client.html = (html && html.body ? html.body : '').trim();
        if (!client.html && oldHtml) client.html = oldHtml;
      }
      catch(e) {
        util.log(2, 'could not fetch HTML', e);
      }

    }

    return client.html;

  }


  // capture element image
  async function getElementImage(client, selector) {

    await client.Page.bringToFront();

    let
      box = await elementMetrics(client, selector),
      screen = '';

    if (box) {

      // capture clipped region
      screen = await client.Page.captureScreenshot({
        format: cfg.screenshot.type.format,
        quality: cfg.screenshot.type.quality,
        clip: {
          x: Math.ceil(Math.max(0, box.content[0])),
          y: Math.ceil(Math.max(0, box.content[1])),
          width: Math.ceil(box.width),
          height: Math.ceil(box.height),
          scale: 1
        }
      });

      screen = (screen && screen.data) || '';

    }

    return (screen);

  }


  // get image data
  async function getImage(client, url) {

    let
      img = {},
      req = client.netReq[url],
      res = req ? client.netRes[req.id] : null;

    if (req && res && res.type === 'image') {

      img.imgData = await client.Network.getResponseBody({ requestId: req.id });
      img.imgData = img.imgData && img.imgData.body ? img.imgData.body : null;

      if (img.imgData) {

        if (res.mime.startsWith('image')) {

          // determine image type from MIME
          img.imgMime = res.mime;
          img.imgExt = res.mime.replace(/^.+\//, '').replace(/\+\w+$/, '').toLowerCase();

        }
        else {

          // determine image type from file extension
          const
            { URL } = require('url'),
            u = new URL(url),
            path = u.pathname,
            ep = path.lastIndexOf('.');

          if (ep >= 0) img.imgExt = path.slice(ep + 1).toLowerCase();

        }

        if (!img.imgExt || img.imgExt === 'jpeg') img.imgExt = 'jpg';

      }

    }

    return img;

  }


  // public methods
  return {
    querySelector: querySelector,
    query: querySelector,
    querySelectorAll: querySelectorAll,
    queryAll: querySelectorAll,
    getContent: getContent,
    clientRun: clientRun,
    getLinks: getLinks,
    documentNodes: documentNodes,
    setHeaders: setHeaders,
    setDeviceSize: setDeviceSize,
    clearCookies: clearCookies,
    loadURL: loadURL,
    runScript: runScript,
    tapElement: tapElement,
    couldRedirect: couldRedirect,
    randomScroll: randomScroll,
    scroll: scroll,
    scrollTo: scrollTo,
    scrollToElement: scrollToElement,
    pageMetrics: pageMetrics,
    elementMetrics: elementMetrics,
    pause: pause,
    screenshot: screenshot,
    screenshotLog: screenshotLog,
    getHTML: getHTML,
    getElementImage: getElementImage,
    getImage: getImage
  };

};
