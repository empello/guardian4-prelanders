// open URL in a new tab
// forked worker process

const
  js = 'javascript:',
  frame = 'iframe',
  cfg = require('../config.json'),
  util = require('./util'),
  clientActions = require('./client-actions');

process.on('message', init => {

  'use strict';

  const
    { opt, ad, pub, url } = init,
    browserConnect = require('./browser-connect')(opt),
    browserActions = require('./browser-actions')(opt),
    scriptProcess = require('./script-process')(opt, 'prelander');

  util.setLogLevel(opt.verbose);
  util.log(2, 'processing ad', (ad+1), url.slice(0,60));

  (async function() {

    // new tab
    let
      tab = await browserConnect.newTarget(),
      imgData = null, imgExt = null, start = new Date();

    // tab not opened?
    if (!tab) {
      await complete(false);
      return;
    }

    if (pub && (url.startsWith(js) || url.startsWith(frame))) {

      // iframe or javascript: link - load publisher page first
      tab.saveBandwidth = true;
      await browserActions.loadURL(tab, pub);

      // reset
      tab.saveBandwidth = opt.minimal;
      tab.loaded = false;
      tab.log = [];
      tab.download = [];

      if (url.startsWith(js)) {

        // execute JS code
        await browserActions.runScript(tab, url.replace(js, ''));

      }
      else {

        if (!opt.minimal) {

          // ensure iframe is moved above other elements
          await browserActions.clientRun(tab, `(${clientActions.showElement})('${url}');`);

          // get advert frame clip from page
          let img = await browserActions.getElementImage(tab, url);

          // move iframe back
          await browserActions.clientRun(tab, `(${clientActions.showElement})('${url}');`);

          if (img) {
            imgData = img;
            imgExt = cfg.screenshot.type.ext;
          }

        }

        // click iframe
        await browserActions.tapElement(tab, url);

      }

      // quit if no new page has loaded
      if (util.urlDomain(pub) === tab.currentDomain) {
        await complete(false);
        return;
      }

    }
    else {

      // standard advert link
      tab.saveBandwidth = opt.minimal;

      // set referer
      if (pub) {

        await browserActions.setHeaders(tab, {
          'Referer': pub,
          'Referrer-Policy': 'unsafe-url'
        });

      }

      await browserActions.loadURL(tab, url);

    }

    // is landing page valid?
    tab.loaded = tabValid();

    if (tab.loaded && !opt.minimal) {

      // prelander script processing (chained scripts are never repeated)
      let omit = [];
      while (!omit.length || omit[omit.length - 1] !== null) {
        omit.push(await scriptProcess(tab, omit, true));
      }

      // final page valid?
      tab.loaded = tabValid();

    }

    if (tab.loaded && cfg.masking.detect) {

      // iframe mask processing
      util.log(2, 'mask detection, ad', (ad + 1), (tab.currentURL || '').slice(0, 60));
      tab.mask = await browserActions.clientRun(tab, `(${clientActions.findMasks})('${tab.currentDomain}','${cfg.blacklist.join(';;')}',${cfg.masking.minVisible},${cfg.masking.minOpacity},${cfg.masking.maxOverlay});`);

    }


    if (tab.loaded) {

      // download link processing
      util.log(2, 'download link detection, ad', (ad + 1), (tab.currentURL || '').slice(0, 60));
      tab.downloadLink = await browserActions.clientRun(tab, `(${clientActions.findDownloads})('${(cfg.download.detect || []).join('|')}');`);

    }


    if (tab.loaded && cfg.download.enabled) {

      // forced download detection
      util.log(2, 'download file detection, ad', (ad + 1), (tab.currentURL || '').slice(0, 60));
      await browserActions.pause(cfg.download.waitfor || 1000);

      // get forced download files
      if (tab.download.length) {
        tab.downloadFiles = await util.fileListUpdated(opt.cache.download, start);
      }

    }

    await complete();


    // is tab valid?
    function tabValid() {
      return tab.loaded && tab.log.length && tab.currentURL && tab.html && tab.html.length >= cfg.minContent && !tab.blacklisted && !tab.hijack && cfg.blacklist.concat(pub || []).every(e => !tab.currentURL.includes(e));
    }


    // complete processing
    async function complete(done = true) {

      util.log(2, 'terminating advert process', (ad+1));

      let info;
      if (done) {

        info = {
          ad: ad,
          loaded: tab.loaded,
          log: tab.log,
          html: tab.html,
          imgData: imgData,
          imgExt: imgExt,
          screen: tab.screen || null,
          screenURL: tab.screenURL || null,
          history: tab.history,
          mask: tab.mask || null,
          download: tab.download,
          downloadFiles: tab.downloadFiles || null,
          downloadLink: tab.downloadLink || null,
          blacklisted: tab.blacklisted
        };

        if (info.loaded) info.title = await browserActions.getContent(tab, 'title');

      }
      else {

        info = {
          ad: ad,
          loaded: false
        };

      }

      // bandwidth used
      info.bandwidth = (tab ? tab.bandwidth : 0);

      // return message
      await process.send(info);

    }

  })();

});


// kill process
process.on('disconnect', function() {

  try {
    process.kill();
  }
  catch (e) {
    util.log(2, 'process could not be killed', e);
  }

});
