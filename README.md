# Empello Guardian4

Guardian4 is a Node.js command-line application which uses Chrome to record advertising journeys from a publisher page. It can run in headless mode and emulate real smartphones.

* [see CHANGELOG.md for revision history](https://bitbucket.org/empello/guardian4/src/master/CHANGELOG.md)
* open MANUAL.html in a browser for documentation ([online copy](http://empello.dh.bytemark.co.uk/g4/MANUAL.html))


## Advances

Guardian4 uses Chrome rather than PhantomJS for Guardian3:

* has all G3 features plus more
* G4 is up to 10x faster
* Chrome is a real, modern browser
* G4 is more difficult to detect
* simpler CLI options
* easier configuration
* cross-platform compatibility
* view journeys in a real browser
* improved bandwidth saving
* low-bandwidth (Pioneer) mode with image spoofing
* invalid journeys are not logged
* postpublisher and prelander scripts with syntax checking


## Installation

Guardian4 is a command-line application which requires:

* [Node.JS 8+](http://nodejs.org/)
* Google Chrome on Windows/Mac or Chromium on Linux

Ubuntu/Debian Chromium `apt` installation:

```bash
sudo apt-get install chromium-browser
```

G4 has been tested with Chrome 62 and above: check using `chromium-browser --version`.

### Guardian4 installation

Clone the repository to a convenient location and install:

```bash
git clone git@bitbucket.org:empello/guardian4.git
cd guardian4
npm install
npm link
```

Ensure the script can be executed on Linux:

```bash
chmod a+x ./guardian4.js
```

## Upgrading

Run the following commands from the `guardian4` folder:

```bash
git pull
npm install
```

## Quickstart

The command `guardian4` or `g4` can be run from any folder.

Example: follow a maximum of three ads from site1.com over the local network on a Samsung Galaxy 9:

```bash
g4 -u site1.com -p local -d gx9 -a 3
```
