# Guardian 4 version history


## 1.17.1

* updated useragent strings based on current browser
* output useragent to JSON result
* focus tab on all browser actions
* improved text conversion
* updated MANUAL.html


## 1.17.0

* new script RUN command
* ad processing order influenced by location on the page
* close alert boxes on Chrome headless
* updated MANUAL.html with improved printing


## 1.16.0

* Google account synchronisation occurs when using a profile folder
* publisher page `--scrape` option
* module updates (verified with Chrome 69)


## 1.15.0

* outputs advert location, dimension and fold metrics


## 1.14.0

* `-U <user:pass>` option for `$username` and `$password` in scripts
* Google login prelander script
* new URL for blacklisting Google adsettings
* random hops use main page content where possible


## 1.13.1

* `--disable-datasaver-prompt` Chrome parameter
* blacklisted Google adsettings
* module upgrades


## 1.13.0

* auto-accept cookie/privacy notices `--yescookies` option
* `agree` set in JSON and log result when auto-accept occurs
* `--maximum` mode to avoid G4 caching
* random scroll and delay after postpublisher and auto-accept
* removed unnecessary cookie-accept postpublisher scripts


## 1.12.2

* proxy HTTP authentication `--auth <user:pass>` option
* set log file `--country` option


## 1.12.1

* capture full-height publisher page option
* FORCED DOWNLOAD URLS changed to ADDITIONAL URLS/TRACKERS (not part of redirect journey)
* JSON outputs advert linkUrl if known
* no screenshot generation on postpublisher scripts
* theguardian.com consent postpublisher script
* Google site search added to blacklist


## 1.12.0

* post-publisher page scripting
* prelander scripts loaded at start when testing
* static FIELD special values when user profile folders are used
* new random password FIELD
* improved script load performance
* console log uses colours to aid readability
* improved bandwidth usage calculation
* increased desktop browser window size


## 1.11.5

* publisher page cached image differs based on proxy (as well as device and URL)
* scripts loaded on first possible use to aid performance
* capture inline base64 and UTF8-encoded SVG ad images
* remove blank ad images
* fixed issue where ad click could fail on frames with an ID containing a slash
* fixed bug which assigned a jpg extension to WebP image files
* fixed issue where JPG compression factor was not used for cropped screenshot
* module updates


## 1.11.4

* devices configuration updates (<https://developers.whatismybrowser.com/> , <https://www.handsetdetection.com/device-detection-database> , <http://vizdevices.yesviz.com/>)
* -h / --hop to randomly jump N pages from the publisher page
* Instagram added to blacklist


## 1.11.3

* ad SVGs saved with .svg extension
* escape slashes in frame IDs so querySelector works


## 1.11.2

* -r / --result output JSON information to stdout and webhook POST


## 1.11.1

* -n / --nofollow mode to record ads only
* -g / --getip to record IP address in logs


## 1.11.0

* new -x / --exclude advertisers option
* allow Google Play but blacklist other Google apps/sites
* blacklisted further analytic systems
* README update


## 1.10.7

* improved ad detection (floated links with no height)
* improved banner image capture, including overlay removal
* improved ad tapping
* optimised screen capture
* blacklist update


## 1.10.6

* fixed operation in Chrome 64
* added headless detection evasion


## 1.10.5

* fix getElementImage (negative x or y values get reset to 0,0)
* reject javascript form submit() links
* prevent iframes triggering download links
* prevent .com URLs and mailto: links being marked as suspicious
* corrected prelander script errors


## 1.10.4

* fixed second-pass iframe capture
* improved ad-click on scaled pages


## 1.10.3

* improved screenshot handling when redirects occur
* timeouts increased for slow prelander redirects
* detect and allow downloads
* check for suspicious download links


## 1.10.2

* Guardian whitelist identification cookie


## 1.10.1

* advert link used even when a target tab is not set
* over-scroll prevention
* fixed errors when ad tab fails to open
* logging improvements


## 1.10.0

* new prelander PROXY command to run on specific connections
* new prelander SHOT command to force a screenshot
* first PROXY and LAND can appear anywhere in a script


## 1.9.5

* captures hidden iframes (e.g. below a cookie warning)
* correctly captures HTML of landing page rather than redirect pages
* improved (HTML to) text content


## 1.9.4

* detect base64-encoded ad images
* frame dimensions used to determine prominence
* framed ad images only used when they cover 80%+ of iframe area
* removed invisible links
* handle application/octet-stream images


## 1.9.3

* fix Chrome 63 journey failures
* attempt to fetch HTML during timeout
* capture framed advert image (also works on non-mobile layouts)
* prelander frame select and click


## 1.9.2

* mask detection
* README update


## 1.9.1

* parsed prelander script caching
* do not record #links in history hijacking
* Windows-compatible npm run clear


## 1.9.0

* uses separate default user profile folder
* permits other profile folders (-i)
* erase cookie option (-e)
* added HTTP headers to spoofed interceptions
* removed landing page scroll (better reliability)
* page presumed loaded only when HTML is present (better reliability)
* do not attempt to get HTML when page has not loaded
* maxResponse and minRedirect timeouts
* streamlined browser-actions.loadURL to better detect 'aw snap'
* do not output blacklisted journeys to failure log
* abort all 404 pages


## 1.8.0

* delay between advert tabs being opened
* output fail.log file containing journey failure information
* improved text content output
* issue browser STOP command prior to timeout
* check if HTML is available and presume page loaded
* always block non-HTTP/S, audio, video and text track downloads
* ensure spoofed Pioneer-mode images cannot be cached and downloaded (prevents blanks)


## 1.7.0

* blacklisted landing page detection and abort
* hijacking detection
* prelander script parsing on hijacked pages
* fixed error generated when tab or browser crashes


## 1.6.0

* image spoofing in minimal (Pioneer) mode
* no prelander loading in minimal mode
* special fields retained across all chained prelanders
* reports bandwidth usage


## 1.5.0

* prelander script chaining
* improved iframe capture
* fix Linux Chromium start-up crash
* updated blacklist


## 1.4.9

* useragent update
* --device own to use current useragent
* improved prelander FIELD special values
* improved landing page timeout handling


## 1.4.8

* special prelander FIELD values


## 1.4.7

* image cropping and blanks removal
* works when run outside main folder


## 1.4.6

* prelander FRAME processing


## 1.4.5

* popup prelander processing
* improved popup capture
* minor log fixes


## 1.4.4

* prelander script import


## 1.4.3

* page title and text extraction
* [SHOT] logging
* LANDING PAGE details
* urls.txt renamed to urls.log
* blocked file downloads


## 1.4.2

* anonymous iframe capture (such as AdWords)


## 1.4.1

* --urljump and --urlcount parameters to limit URL processing
* improved scrolling and tap gesture functionality
* stopped prelander processing in minimal (Pioneer) mode
* minor tweaks to prelander scripts


## 1.4.0

* pre-lander script processing
* repeated screenshot removal


## 1.3.1

* fixed --open option
* browser-connect efficiency improvements


## 1.3.0

* restructure for prelander script testing and processing
* history (back button) manipulation recording
* number of ads to follow can be set as a command-line parameter
* caches publisher page per device
* browser resizing
* kills orphaned workers
* log write timeout


## 1.2.0

* multi-URL processing
* Node.js version check
* HTTP Referrer sent on advert tab
* improved error detection
* module update


## 1.1.2

* blocked market: and sms: URLs
* over-scroll prevention
* crop HTML length fix
* tab closure after processing
* prevent browser crash errors


## 1.1.1

* initial bandwidth saving mode (Pioneer)
* publisher page image caching
* ad detection improvements
* verified javascript: link processing
* scroll delay on landing page
* improved HTML recording
* improved popup handling
* crop HTML length
* prevent errors when browser fails
* -v 0 (zero verbosity) fixed


## 1.1.0

* refactoring of codebase
* javascript: link following
* complex client-side redirection
* pop-up logging
* capture full-page screenshot
* webhook call
* enable/disable ad-blocking (when available in Chrome)
* improved concurrency
* faster processing


## 1.0.0

* Initial MVP release.
