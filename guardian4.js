#!/usr/bin/env node

/*
Guardian4
*/

'use strict';

// check Node.js support
const
  pkg = require('./package.json'),
  util = require('./lib/util');

if (util.versionToInt(process.version) < util.versionToInt(pkg.guardian.nodejs)) {
  util.log(0, `${pkg.name} requires Node.js v${pkg.guardian.nodejs} or above.`);

  process.exit(1);
  return;
}

// global constants
const
  opt = require('./lib/opt')(process.argv),
  browser = require('./lib/browser');

let
  quitting = false, // G4 quitting
  adCount = 0;      // adverts successfully processed

// command line error
if (!opt.valid) {
  quit(1);
  return;
}

util.setLogLevel(opt.verbose);

// main loop
(async function() {

  util.log(1, '\n', '#'.repeat(60));
  util.log(2, pkg.name, pkg.version);

  // load scripts
  let scriptLoad = require('./lib/script-load');
  opt.postpublisher = await scriptLoad({ type: 'postpublisher', cache: opt.cache.postpublisher, user: opt.userfolder, verbose: opt.verbose });

  // prelander scripts loaded at start when testing
  if (opt.test && !opt.minimal) {

    opt.prelander = await scriptLoad({ type: 'prelander', cache: opt.cache.prelander, user: opt.userfolder, verbose: opt.verbose });

  }

  // browser failure event
  browser.event.once('close', async code => {
    await quit(code);
    return;
  });

  // start browser
  await browser.start();

  // processing modules
  const
    processURL = require('./lib/process-url')(opt),
    processAds = require('./lib/process-ads')(opt),
    writeLogs = require('./lib/writelogs');

  let
    resultJSON = {
      profile: opt.userfolder || 'default',
      proxy: opt.proxy || 'local',
      device: opt.deviceCode,
      devicename: opt.device.name,
      deviceua: '',
      log: opt.logfolder,
      run: []
    },
    bandwidth = 0;

  // process all URLs
  while (opt.url.length) {

    // load publisher page and locate ads
    let pubInfo = await processURL.start(opt.url.shift());
    if (!pubInfo) continue;

    // process ad journeys
    let { publisher, adList } = pubInfo;
    bandwidth += (publisher && publisher.bandwidth || 0); // publisher bandwidth

    if (!publisher || !publisher.currentURL || !adList || (!opt.scrape && !adList.length)) continue;

    if (!opt.nofollow && adList.length) {
      await processAds.start(adList, opt.test || adList[0].type === 'hijack' ? null : publisher.currentURL);
      bandwidth +=  adList.reduce((s, v) => s + (v.bandwidth || 0), 0); // ad-follow bandwidth
    }

    // write journey logs
    let { adId, result } = await writeLogs(opt, publisher, adList);
    if (adList.length) util.log(1, '\x1b[33m\x1b[1m[-----', (opt.nofollow ? 'ADS LOGGED:' : 'SUCCESSFUL JOURNEYS:'), adId, 'ABORTS:', (adList.length - adId), '-----]\x1b[0m');

    if (result) resultJSON.run.push(result);
    adCount += adId;

  }

  // output bandwidth used
  util.log(1, '\x1b[33m\x1b[1m[----- BANDWIDTH USED:', bandwidth.toLocaleString(), 'bytes |', Math.ceil(bandwidth/1024).toLocaleString(), 'kb |', Math.ceil(bandwidth/1048576).toLocaleString(), 'MB -----]\x1b[0m');

  // finish
  resultJSON.deviceua = opt.device.useragent;
  await quit(0, opt.result ? JSON.stringify(resultJSON) : null);
  return;

})();


// quit Guardian
async function quit(code = 0, result) {

  if (quitting) return;
  quitting = true;

  // terminate
  browser.stop();

  // call webhook
  if (adCount && opt.webhook) {
    let hook = await util.webRequest(opt.webhook, result);
    util.log(1, 'webhook call', (hook ? 'successful' : 'failed'), opt.webhook.slice(0, 40));
  }

  // display result
  if (result) {
    util.log(1, '[----- RESULT -----]');
    console.log(result);
  }

  process.exit(code);

}
